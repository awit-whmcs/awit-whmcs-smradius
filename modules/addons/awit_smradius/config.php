<?php

/**
 * @method autoload
 * Loads the given class from lib/
 *
 * @param $className
 */

function autoload($className)
{
	$className = ltrim($className, '\\');
	$fileName  = '';
	$namespace = '';
	if ($lastNsPos = strrpos($className, '\\')) {
		$namespace = substr($className, 0, $lastNsPos);
		$className = substr($className, $lastNsPos + 1);
		$fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
	}
	$fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

	$fileName = dirname(__FILE__) . '/lib/' . $fileName;
	if (!file_exists($fileName)) {
		throw new Exception("Class file not found \"$fileName\"\n");
	}

	require_once $fileName;
}

// autoloading required classes
autoload('AWIT_WHMCS_Smradius');
autoload('AWIT_WHMCS_Data_Array');
autoload('AWIT_WHMCS_Data_Schema');
autoload('AWIT_WHMCS_Data_Config');
autoload('AWIT_WHMCS_Data_Connection');
autoload('AWIT_WHMCS_Util');
autoload('AWIT_WHMCS_Html_Datagrid');
autoload('AWIT_WHMCS_Html_Form');
autoload('AWIT_WHMCS_Data_FormsManager');
autoload('AWIT_WHMCS_Data_Drivers_CakePhpRest');
autoload('AWIT_WHMCS_Data_Drivers_Mysql');

// Setting global config variable
$config = array();

// Constructing globals
$objSmradius = new AWIT_WHMCS_Smradius();
$whmcsConfig = $objSmradius->getConfigArray();

// Setting CakePHP REST Config Array
$config['cakephp-rest-config'] = array(
	'url' => $whmcsConfig["smradius_url"],
	'cookie_file' => $whmcsConfig["smradius_cookie_file"],
	'auth' => array (
		'username' => array('data[WebuiUser][Username]' => $whmcsConfig["smradius_username"]),
		'password' => array('data[WebuiUser][Password]' => $whmcsConfig["smradius_password"]),
		'login-query-string' => '/webui_users/login',
		'session-user-id' => 'User.ID',
	)
);



// Setting CakePHP REST Schema Array
$config['cakephp-rest-schema'] = array(
	// Default query string
	'read-query-string' => '/users/index',



	// Users
	'/^Users\/users\/\d*\/User/' => array (
		'table' => 'tbl_awit_smradius_users',
		'fields' => array(
			'ID' => 'id',
			'Username' => 'data[User][Username]',
			'Password' => 'data[User][Password]',
			'Disabled' => 'data[User][Disabled]',
		),
		'uniquekey' => array('username'),
	),
	'/^Users\/users$/' => array (
		'no-data-message' => 'No Users Exist',
	),



	// User Attributes
	'/userAttributes\/\d*\/UserAttribute/' => array (
		'table' => 'tbl_awit_smradius_attributes',
		'fields' => array(
			'ID' => 'id',
			'Name' => 'data[UserAttribute][Name]',
			'Operator' => 'data[UserAttribute][Operator]',
			'Value' => 'data[UserAttribute][Value]',
			'Disabled' => 'data[UserAttribute][Disabled]'
		)
	),
	'/^UserAttributes\/userAttributes$/' => array (
		'no-data-message' => 'No attribues exist for this user',
	),



	// User Groups
	'/UserGroup\/UserGroup\/\d*\/UserGroup/' => array (
		'table' => 'tbl_awit_smradius_attributes',
		'fields' => array(
			'ID' => 'id',
			'group' => 'data[UserGroup][group]',
			'Comment' => 'data[UserGroup][Comment]',
			'Disabled' => 'data[UserGroup][Disabled]',
			'GroupID' => 'data[UserGroup][GroupID]',
			'UserID' => 'data[UserGroup][UserID]',
		)
	),
	'/^UserGroup\/UserGroup$/' => array (
		'no-data-message' => 'This user does not belong to any groups',
	),
	// UserGroup Assign Select List
	'/^UserGroup\/arr$/' => array (
		'array' => 'UserGroup/arr'
	),



	// User Topups
	'/UserTopups\/topups(\/\d*|)\/UserTopup/' => array (
		'table' => 'tbl_awit_smradius_attributes',
		'fields' => array(
			'ID' => 'id',
			'Type' => 'data[UserTopup][Type]',
			'Value' => 'data[UserTopup][Value]',
			'ValidFrom' => 'data[UserTopup][valid_from]',
			'ValidTo' => 'data[UserTopup][valid_to]',
		)
	),
	// Handle Empty Attributes
	'/^UserTopups\/topups$/' => array (
		'no-data-message' => 'No topups exist for this user',
	),



	// Groups
	'/groups\/\d*\/Group/' => array (
		'table' => 'tbl_awit_smradius_groups',
		'fields' => array(
			'ID' => 'id',
			'Name' => 'data[Group][Name]',
			'Priority' => 'data[Group][Priority]',
			'Disabled' => 'data[Group][Disabled]',
			'Comment' => 'data[Group][Comment]',
		),
		'uniquekey' => array('groupname'),
	),
	'/^groups$/' => array (
		'no-data-message' => 'No Groups Exist',
	),

	// Group Attributes
	'/groupAttributes\/\d*\/GroupAttribute/' => array (
		'table' => 'tbl_awit_smradius_attributes',
		'fields' => array(
			'ID' => 'id',
			'Name' => 'data[GroupAttribute][Name]',
			'Operator' => 'data[GroupAttribute][Operator]',
			'Value' => 'data[GroupAttribute][Value]',
			'Disabled' => 'data[GroupAttribute][Disabled]'
		)
	),
	// Handle Empty Attributes
	'/^GroupAttributes\/groupAttributes$/' => array (
		'no-data-message' => 'No attribues exist for this group',
	),


	// Group Members
	'/^GroupMembers\/GroupMember\/\d*\/GroupMember/' => array (
		'table' => 'tbl_awit_smradius_attributes',
		'fields' => array(
			'ID' => 'id',
			'UserName' => 'data[GroupMember][UserName]',
			'Disabled' => 'data[GroupMember][Disabled]'
		)
	),
	// Handle Empty Attributes
	'/^GroupMembers\/GroupsMember$/' => array (
		'no-data-message' => 'There are no members in this group',
	),



	// Realms
	'/^Realms\/realm\/\d*\/Realm/' => array (
		'table' => 'tbl_awit_smradius_realms',
		'fields' => array(
			'ID' => 'id',
			'Name' => 'data[Realm][Name]',
			'Disabled' => 'data[Realm][Disabled]',
		),
		'uniquekey' => array('realmname'),
	),
	'/^Realms\/realm$/' => array (
		'no-data-message' => 'No Realms Exist',
	),

	// Realm Attributes
	'/RealmAttributes\/realmAttributes\/\d*\/RealmAttribute/' => array (
		'table' => 'tbl_awit_smradius_attributes',
		'fields' => array(
			'ID' => 'id',
			'Name' => 'data[RealmAttribute][Name]',
			'Operator' => 'data[RealmAttribute][Operator]',
			'Value' => 'data[RealmAttribute][Value]',
			'Disabled' => 'data[RealmAttribute][Disabled]'
		)
	),
	// Handle Empty Attributes
	'/^RealmAttributes\/realmAttributes$/' => array (
		'no-data-message' => 'No attribues exist for this realm',
	),

	// Realm Members
	'/^RealmMembers\/realmMember\/\d*\/RealmMember/' => array (
		'table' => 'tbl_awit_smradius_attributes',
		'fields' => array(
			'ID' => 'id',
			'clientName' => 'data[RealmMember][clientName]',
			'Disabled' => 'data[RealmMember][Disabled]'
		)
	),
	// Handle Empty Attributes
	'/^RealmMembers\/realmsMember$/' => array (
		'no-data-message' => 'There are no members in this realm',
	),



	// Clients
	'/^Clients\/client\/\d*\/Client/' => array (
		'table' => 'tbl_awit_smradius_clients',
		'fields' => array(
			'ID' => 'id',
			'Name' => 'data[Client][Name]',
			'AccessList' => 'data[Client][AccessList]',
			'Disabled' => 'data[Client][Disabled]',
		),
		'uniquekey' => array('clientname'),
	),
	'/^Clients\/client$/' => array (
		'no-data-message' => 'No Clients Exist',
	),

	// Client Attributes
	'/ClientAttributes\/clientAttributes\/\d*\/ClientAttribute/' => array (
		'table' => 'tbl_awit_smradius_attributes',
		'fields' => array(
			'ID' => 'id',
			'Name' => 'data[ClientAttribute][Name]',
			'Operator' => 'data[ClientAttribute][Operator]',
			'Value' => 'data[ClientAttribute][Value]',
			'Disabled' => 'data[ClientAttribute][Disabled]'
		)
	),
	// Handle Empty Attributes
	'/^ClientAttributes\/clientAttributes$/' => array (
		'no-data-message' => 'No attribues exist for this client',
	),

	// Client Realms
	'/^ClientRealms\/clientRealms\/\d*\/ClientRealm/' => array (
		'table' => 'tbl_awit_smradius_attributes',
		'fields' => array(
			'ID' => 'id',
			'realmName' => 'data[ClientRealm][realmName]',
			'Disabled' => 'data[ClientRealm][Disabled]'
		)
	),
	// Handle Empty Attributes
	'/^ClientRealms\/clientsRealm$/' => array (
		'no-data-message' => 'There are no realms in this client',
	),
	// Client Realms Select List
	'/^ClientRealms\/arr$/' => array (
		'array' => 'ClientRealms/arr'
	),

);



// Configuring forms
$config['html-forms'] = array(
	// Clients
	'mg-clients' => array (
		'data[Client][Name]' => array(
			'label' => 'Name',
			'type' => 'text',
			'name' => 'name',
		),
		'data[Client][AccessList]' => array(
			'label' => 'Access List',
			'type' => 'text',
			'name' => 'accesslist',
		),
		'data[Client][Disabled]' => array(
			'label' => 'Disabled',
			'type' => 'checkbox',
			'name' => 'disabled',
		),
		'submit' => array(
			'label' => '   Save   ',
			'type' => 'submit',
			'name' => 'submit'
		)
	),

	// Client Attributes
	'mg-clients/attributes' => array (
		'data[ClientAttribute][Name]' => array(
			'label' => 'Name',
			'type' => 'text',
			'name' => 'name',
		),
		'data[ClientAttribute][Operator]' => array(
			'label' => 'Operator',
			'type' => 'text',
			'name' => 'operator',
		),
		'data[ClientAttribute][Value]' => array(
			'label' => 'Value',
			'type' => 'text',
			'name' => 'value',
		),
		'data[ClientAttribute][Disabled]' => array(
			'label' => 'Disabled',
			'type' => 'checkbox',
			'name' => 'disabled',
		),
		'submit' => array(
			'label' => '   Save   ',
			'type' => 'submit',
			'name' => 'submit'
		)
	),


	// Client Members/Realms
	'mg-clients/realm' => array (
		'data[ClientRealm][Type]' => array(
			'label' => 'Name',
			'type' => 'select',
			'name' => 'name',
			'cake::query-string' => 'client_realms/add/{id}',
		),
		'submit' => array(
			'label' => '   Save   ',
			'type' => 'submit',
			'name' => 'submit'
		)
	),





	// Realms
	'mg-realms' => array (
		'data[Realm][Name]' => array(
			'label' => 'Name',
			'type' => 'text',
			'name' => 'name',
		),
		'data[Realm][Disabled]' => array(
			'label' => 'Disabled',
			'type' => 'checkbox',
			'name' => 'disabled',
		),
		'submit' => array(
			'label' => '   Save   ',
			'type' => 'submit',
			'name' => 'submit'
		)
	),

	// Realm Attributes
	'mg-realms/attributes' => array (
		'data[RealmAttribute][Name]' => array(
			'label' => 'Name',
			'type' => 'text',
			'name' => 'name',
		),
		'data[RealmAttribute][Operator]' => array(
			'label' => 'Operator',
			'type' => 'text',
			'name' => 'operator',
		),
		'data[RealmAttribute][Value]' => array(
			'label' => 'Value',
			'type' => 'text',
			'name' => 'value',
		),
		'data[RealmAttribute][Disabled]' => array(
			'label' => 'Disabled',
			'type' => 'checkbox',
			'name' => 'disabled',
		),
		'submit' => array(
			'label' => '   Save   ',
			'type' => 'submit',
			'name' => 'submit'
		)
	),


	// Groups
	'mg-groups' => array (
		'data[Group][Name]' => array(
			'label' => 'Name',
			'type' => 'text',
			'name' => 'name',
		),
		'data[Group][Priority]' => array(
			'label' => 'Priority',
			'type' => 'text',
			'name' => 'priority',
		),
		'data[Group][Disabled]' => array(
			'label' => 'Disabled',
			'type' => 'checkbox',
			'name' => 'disabled',
		),
		'data[Group][Comment]' => array(
			'label' => 'Comment',
			'type' => 'text',
			'name' => 'comment',
		),
		'submit' => array(
			'label' => '   Save   ',
			'type' => 'submit',
			'name' => 'submit'
		)
	),

	// Group Attributes
	'mg-groups/attributes' => array (
		'data[GroupAttribute][Name]' => array(
			'label' => 'Name',
			'type' => 'text',
			'name' => 'name',
		),
		'data[GroupAttribute][Operator]' => array(
			'label' => 'Operator',
			'type' => 'text',
			'name' => 'operator',
		),
		'data[GroupAttribute][Value]' => array(
			'label' => 'Value',
			'type' => 'text',
			'name' => 'value',
		),
		'data[GroupAttribute][Disabled]' => array(
			'label' => 'Disabled',
			'type' => 'checkbox',
			'name' => 'disabled',
		),
		'submit' => array(
			'label' => '   Save   ',
			'type' => 'submit',
			'name' => 'submit'
		)
	),


	// Users
	'mg-users' => array (
		'data[User][Username]' => array(
			'label' => 'Username',
			'type' => 'text',
			'name' => 'username',
		),
		'data[User][Password]' => array(
			'label' => 'Password',
			'type' => 'text',
			'name' => 'password',
		),
		'data[User][Disabled]' => array(
			'label' => 'Disabled',
			'type' => 'checkbox',
			'name' => 'disabled',
		),
		'submit' => array(
			'label' => '   Save   ',
			'type' => 'submit',
			'name' => 'submit'
		)
	),

	// User Attributes
	'mg-users/attributes' => array (
		'data[UserAttribute][Name]' => array(
			'label' => 'Name',
			'type' => 'text',
			'name' => 'name',
		),
		'data[UserAttribute][Operator]' => array(
			'label' => 'Operator',
			'type' => 'text',
			'name' => 'operator',
		),
		'data[UserAttribute][Value]' => array(
			'label' => 'Value',
			'type' => 'text',
			'name' => 'value',
		),
		'data[UserAttribute][Disabled]' => array(
			'label' => 'Disabled',
			'type' => 'checkbox',
			'name' => 'disabled',
		),
		'submit' => array(
			'label' => '   Save   ',
			'type' => 'submit',
			'name' => 'submit'
		)
	),

	// User Groups
	'mg-users/user_groups' => array (
		'data[UserGroup][GroupID]' => array(
			'label' => 'Name',
			'type' => 'select',
			'name' => 'name',
			'cake::query-string' => 'user_groups/add/{id}',
		),
		'submit' => array(
			'label' => '   Save   ',
			'type' => 'submit',
			'name' => 'submit'
		)
	),

	// User Topups
	'mg-users/topups' => array (
		'data[UserTopup][Type]' => array(
			'label' => 'Type',
			'type' => 'select',
			'options' => array(
				'1' => 'Traffic',
				'2' => 'Uptime',
			),
			'name' => 'name',
		),
		'data[UserTopup][Value]' => array(
			'label' => 'Value',
			'type' => 'text',
			'name' => 'value',
		),
		'data[UserTopup][valid_from]' => array(
			'label' => 'Valid From',
			'type' => 'date',
			'name' => 'valid_from',
		),
		'data[UserTopup][valid_to]' => array(
			'label' => 'Valid To',
			'type' => 'date',
			'name' => 'valid_to',
		),
		'submit' => array(
			'label' => '   Save   ',
			'type' => 'submit',
			'name' => 'submit'
		)
	)
);

# vim: ts=4
