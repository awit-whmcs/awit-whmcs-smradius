<?php
/**
 * AWIT SMRADIUS - Module to Facilitate SMRadius Integration
 * Copyright (c) 2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Make sure we not being accssed directly
if (!defined("WHMCS")) {
	die("This file cannot be accessed directly");
}

// Reading WHMCS Config
require_once('config.php');

// Creating the entityManager
$entityManager = new AWIT_WHMCS_Data_Connection();
$entityManager->setDriver('AWIT_WHMCS_Data_Drivers_CakePhpRest');
$entityManager->setConfig($config['cakephp-rest-config']);
$entityManager->setSchema($config['cakephp-rest-schema']);

// Setting up forms
$formsManager = new AWIT_WHMCS_Html_FormsManager();
$formsManager->setEntityManager($entityManager);

// Defining forms
$formsManager->setForms($config['html-forms']);

// Addon configuration
function awit_smradius_config()
{
	// Configuration
	$configarray = array(
		"name" => "AWIT SMRADIUS",
		"description" => "Module to facilitate SMRADIUS integration",
		"version" => "0.1",
		"author" => "AllWorldIT",
		"language" => "english",
		"fields" => array(
			// SMRadius Host
			"smradius_url" => array (
				"FriendlyName" => "SMRadius URL",
				"Description" => "SMRadius Server Hostname",
				"Type" => "text", "Size" => "25",
				"Default" => ""
			),
			// SMRadius Admin User
			"smradius_username" => array (
				"FriendlyName" => "SMRadius Admin Username",
				"Description" => "SMRadius Admin Username",
				"Type" => "text", "Size" => 25,
				"Default" => "admin"
			),
			// SMRadius Admin Password
			"smradius_password" => array (
				"FriendlyName" => "SMRadius Admin Password",
				"Description" => "SMRadius Admin Password",
				"Type" => "text", "Size" => "25",
				"Default" => ""
			),
			// SMRadius SSL Cert
			"smradius_ssl_cert" => array (
				"FriendlyName" => "SMRadius SSL Cert",
				"Description" => "Path to SSL Certificate (Optional, Leave empty to disable)",
				"Type" => "text", "Size" => "25",
				"Default" => ""
			),
		),
	);

	return $configarray;
}



// Client Realms Delete Action
function processClientRealmsDelete($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Handle delete subaction for realm
	$link .= '&do[]=realm&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/client_realms/remove/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$data = $entityManager->writeData();
	if ($entityManager->errors($data) === false) {
		AWIT_WHMCS_Util::logSuccess('Successfully removed realm from client');
	}

	$id = $id[0];
	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/client_realms/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/client_realms/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[ClientRealm][realmName]');
	$grid->setLocalActions('delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);

	$output = $grid->drawDatagrid(
		"Client Realms",
		"Realms of this client",
		array(),
		array(
			'id' => 'ID',
			'data[ClientRealm][realmName]' => 'Name',
			'data[ClientRealm][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Client Realms Add Action
function processClientRealmsAdd($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Link to submit back to
	$link .= '&do[]=realm&id[]=' . urlencode($id);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/client_realms/add/' . urlencode($id));
	$form = new AWIT_WHMCS_Html_Form();
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action . '/' . $do[0],
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action . '/' . $do[0]));

	// Processing form
	$output = $form->drawForm("Client Realms", "Assign Realm");

	if ($output !== false) {
		echo $output;
	}

}



// Client Realms List Action
function processClientRealmsList($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	$link .= '&do[]=realm&id[]=' . urlencode($id);

	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/client_realms/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/client_realms/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[ClientRealm][clientName]');
	$grid->setLocalActions('delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);

	$grid->setCustomMenuActions(
		array(
			array(
				'title' => 'Assign Realm To Client',
				'label' => 'Assign Realm',
				'class' => 'btn-success',
				'link' => $link . '&do[]=add',
			),
		)
	);

	$output = $grid->drawDatagrid(
		"Client Realms",
		"Realms of this client",
		array(),
		array(
			'id' => 'ID',
			'data[ClientRealm][realmName]' => 'Name',
			'data[ClientRealm][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Client Attributes Edit Action
function processClientAttributesEdit($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Setting link to submit back to, doPath type
	$link .= '&do[]=attributes&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	if (!empty($page)) {
		$entityManager->setReadQueryString('/client_attributes/index/' . urlencode($id[0]) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/client_attributes/index/' . urlencode($id[0]));
	}

	$entityManager->setWriteQueryString('/client_attributes/edit/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$form = new AWIT_WHMCS_Html_Form($id[1]);
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action . '/' . $do[0],
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action . '/' . $do[0]));

	// Processing form
	$output = $form->drawForm("Client Attributes", "Edit Client Attributes");

	if ($output !== false) {
		echo $output;
	}

}



// Client Attributes Delete Action
function processClientAttributesDelete($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Handle delete subaction for attributes
	$link .= '&do[]=attributes&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/client_attributes/remove/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$data = $entityManager->writeData();
	if ($entityManager->errors($data) === false) {
		AWIT_WHMCS_Util::logSuccess('Successfully deleted Client Attribute');
	}

	$id = $id[0];
	// Handle pagination
	if (isset($_REQUEST['page'])) {
		$page = $_REQUEST['page'];
		$entityManager->setReadQueryString('/client_attributes/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/client_attributes/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[ClientAttribute][Name]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$output = $grid->drawDatagrid(
		"Client Attributes",
		"Attributes for the client",
		array(),
		array(
			'id' => 'ID',
			'data[ClientAttribute][Name]' => 'Name',
			'data[ClientAttribute][Operator]' => 'Operator',
			'data[ClientAttribute][Value]' => 'Value',
			'data[ClientAttribute][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Client Attributes Add Action
function processClientAttributesAdd($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Link to submit back to
	$link .= '&do[]=attributes&id[]=' . urlencode($id);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/client_attributes/add/' . urlencode($id));
	$form = new AWIT_WHMCS_Html_Form();
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action . '/' . $do[0],
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action . '/' . $do[0]));

	// Processing form
	$output = $form->drawForm("Clients Attributes", "Create Client Attributes");

	if ($output !== false) {
		echo $output;
	}

}



// Client Attributes List Action
function processClientAttributesList($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	$link .= '&do[]=attributes&id[]=' . urlencode($id);

	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/client_attributes/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/client_attributes/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[ClientAttribute][Name]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$output = $grid->drawDatagrid(
		"Client Attributes",
		"Attributes for the client",
		array(),
		array(
			'id' => 'ID',
			'data[ClientAttribute][Name]' => 'Name',
			'data[ClientAttribute][Operator]' => 'Operator',
			'data[ClientAttribute][Value]' => 'Value',
			'data[ClientAttribute][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Client List Action
function processClientsList($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/clients/index?page=' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/clients/index');
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[Client][Name]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$grid->setCustomLocalActions(
		array(
			array(
				'title' => 'Client Attributes',
				'image' => 'images/icons/invoices.png',
				'link' => $link . '&do=attributes',
			),
			array(
				'title' => 'Client Realms',
				'image' => 'images/icons/domains.png',
				'link' => $link . '&do=realm',
			),
		)
	);

	$output = $grid->drawDatagrid(
		"Clients",
		"Clients List",
		array(),
		array(
			'id' => 'ID',
			'data[Client][Name]' => 'Name',
			'data[Client][AccessList]' => 'Access List',
			'data[Client][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Client Delete Action
function processClientsDelete($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/clients/remove/' . urlencode($id));
	$data = $entityManager->writeData();
	if ($entityManager->errors($data) === false) {
		AWIT_WHMCS_Util::logSuccess('Successfully deleted Client');
	}

	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/clients/index?page=' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/clients/index');
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[Client][Name]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$grid->setCustomLocalActions(
		array(
			array(
				'title' => 'Attributes',
				'image' => 'images/icons/invoices.png',
				'link' => $link . '&do=attributes',
			),
			array(
				'title' => 'Client Realms',
				'image' => 'images/icons/domains.png',
				'link' => $link . '&do=realm',
			),
		)
	);

	$output = $grid->drawDatagrid(
		"Clients",
		"Clients List",
		array(),
		array(
			'id' => 'ID',
			'data[Client][Name]' => 'Name',
			'data[Client][AccessList]' => 'Access List',
			'data[Client][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Client Add Action
function processClientsAdd($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/clients/add');
	$form = new AWIT_WHMCS_Html_Form();
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action,
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action));

	// Processing form
	$output = $form->drawForm("Clients", "Create Client");

	if ($output !== false) {
		echo $output;
	}

}



// Client Edit Action
function processClientsEdit($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Set the link to return to
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	if (!empty($page)) {
		$entityManager->setReadQueryString('/clients/index/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/clients/index');
	}

	$entityManager->setWriteQueryString('/clients/edit/' . urlencode($id));
	$form = new AWIT_WHMCS_Html_Form($id);
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action,
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action));

	// Processing form
	$output = $form->drawForm("Clients", "Edit Client");

	if ($output !== false) {
		echo $output;
	}

}



// Realm Members Delete Action
function processRealmMembersDelete($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Handle delete subaction for member
	$link .= '&do[]=member&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/realm_members/remove/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$data = $entityManager->writeData();
	if ($entityManager->errors($data) === false) {
		AWIT_WHMCS_Util::logSuccess('Successfully removed member from realm');
	}

	$id = $id[0];
	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/realm_members/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/realm_members/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('name');
	$grid->setLocalActions('delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);

	$output = $grid->drawDatagrid(
		"Realm Members",
		"Members of this realm",
		array(),
		array(
			'id' => 'ID',
			'data[RealmMember][UserName]' => 'Name',
			'data[RealmMember][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}


// Realm Members List Action
function processRealmMembersList($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	$link .= '&do[]=member&id[]=' . urlencode($id);

	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/realm_members/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/realm_members/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[RealmMember][clientName]');
	$grid->setLocalActions('delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);

	$output = $grid->drawDatagrid(
		"Realm Members",
		"Members of this realm",
		array(),
		array(
			'id' => 'ID',
			'data[RealmMember][clientName]' => 'Username',
			'data[RealmMember][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Realms Delete Action
function processRealmsDelete($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/realms/remove/' . urlencode($id));
	$data = $entityManager->writeData();
	if ($entityManager->errors($data) === false) {
		AWIT_WHMCS_Util::logSuccess('Successfully deleted Realm');
	}

	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/realms/index?page=' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/realms/index');
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[Realm][Name]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$grid->setCustomLocalActions(
		array(
			array(
				'title' => 'Attributes',
				'image' => 'images/icons/invoices.png',
				'link' => $link . '&do=attributes',
			),
			array(
				'title' => 'Realm Members',
				'image' => 'images/icons/clients.png',
				'link' => $link . '&do=member',
			),
		)
	);

	$output = $grid->drawDatagrid(
		"Realms",
		"Realms List",
		array(),
		array(
			'id' => 'ID',
			'data[Realm][Name]' => 'Name',
			'data[Realm][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}


// Realms Add Action
function processRealmsAdd($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/realms/add');
	$form = new AWIT_WHMCS_Html_Form();
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action,
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action));

	// Processing form
	$output = $form->drawForm("Realms", "Create Realm");

	if ($output !== false) {
		echo $output;
	}

}



// Realm Attributes Delete Action
function processRealmAttributesDelete($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Handle delete subaction for attributes
	$link .= '&do[]=attributes&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/realm_attributes/remove/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$data = $entityManager->writeData();
	if ($entityManager->errors($data) === false) {
		AWIT_WHMCS_Util::logSuccess('Successfully deleted Realm Attribute');
	}

	$id = $id[0];
	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/realm_attributes/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/realm_attributes/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[RealmAttribute][Name]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$output = $grid->drawDatagrid(
		"Realm Attributes",
		"Attributes for the realm",
		array(),
		array(
			'id' => 'ID',
			'data[RealmAttribute][Name]' => 'Name',
			'data[RealmAttribute][Operator]' => 'Operator',
			'data[RealmAttribute][Value]' => 'Value',
			'data[RealmAttribute][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Realm Attributes Edit Action
function processRealmAttributesEdit($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Setting link to submit back to, doPath type
	$link .= '&do[]=attributes&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	if (!empty($page)) {
		$entityManager->setReadQueryString('/realm_attributes/index/' . urlencode($id[0]) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/realm_attributes/index/' . urlencode($id[0]));
	}

	$entityManager->setWriteQueryString('/realm_attributes/edit/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$form = new AWIT_WHMCS_Html_Form($id[1]);
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action . '/' . $do[0],
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action . '/' . $do[0]));

	// Processing form
	$output = $form->drawForm("Realm Attributes", "Edit Realm Attributes");

	if ($output !== false) {
		echo $output;
	}

}



// Realm Attributes Add Action
function processRealmAttributesAdd($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Link to submit back to
	$link .= '&do[]=attributes&id[]=' . urlencode($id);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/realm_attributes/add/' . urlencode($id));
	$form = new AWIT_WHMCS_Html_Form();
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action . '/' . $do[0],
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action . '/' . $do[0]));

	// Processing form
	$output = $form->drawForm("Realms Attributes", "Create Realm Attributes");

	if ($output !== false) {
		echo $output;
	}

}


// Realm Attributes List Action
function processRealmAttributesList($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	$link .= '&do[]=attributes&id[]=' . urlencode($id);

	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/realm_attributes/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/realm_attributes/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[RealmAttribute][Name]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$output = $grid->drawDatagrid(
		"Realm Attributes",
		"Attributes for the realm",
		array(),
		array(
			'id' => 'ID',
			'data[RealmAttribute][Name]' => 'Name',
			'data[RealmAttribute][Operator]' => 'Operator',
			'data[RealmAttribute][Value]' => 'Value',
			'data[RealmAttribute][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Realms Edit Action
function processRealmsEdit($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Set the link to return to
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	if (!empty($page)) {
		$entityManager->setReadQueryString('/realms/index/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/realms/index');
	}

	$entityManager->setWriteQueryString('/realms/edit/' . urlencode($id));
	$form = new AWIT_WHMCS_Html_Form($id);
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action,
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action));

	// Processing form
	$output = $form->drawForm("Realms", "Edit Realm");

	if ($output !== false) {
		echo $output;
	}

}



// Realms List Action
function processRealmsList($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/realms/index?page=' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/realms/index');
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[Realm][Name]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$grid->setCustomLocalActions(
		array(
			array(
				'title' => 'Realm Attributes',
				'image' => 'images/icons/invoices.png',
				'link' => $link . '&do=attributes',
			),
			array(
				'title' => 'Realm Members',
				'image' => 'images/icons/clients.png',
				'link' => $link . '&do=member',
			),
		)
	);

	$output = $grid->drawDatagrid(
		"Realms",
		"Realms List",
		array(),
		array(
			'id' => 'ID',
			'data[Realm][Name]' => 'Name',
			'data[Realm][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Group Members List Action
function processGroupMembersList($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	$link .= '&do[]=member&id[]=' . urlencode($id);

	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/group_member/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/group_member/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[GroupMember][UserName]');
	$grid->setLocalActions('delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);

	$output = $grid->drawDatagrid(
		"Group Members",
		"Members of this group",
		array(),
		array(
			'id' => 'ID',
			'data[GroupMember][UserName]' => 'Username',
			'data[GroupMember][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Group Members Delete Action
function processGroupMembersDelete($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Handle delete subaction for member
	$link .= '&do[]=member&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/group_member/remove/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$data = $entityManager->writeData();
	if ($entityManager->errors($data) === false) {
		AWIT_WHMCS_Util::logSuccess('Successfully removed member from group');
	}

	$id = $id[0];
	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/group_member/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/group_member/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('name');
	$grid->setLocalActions('delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);

	$output = $grid->drawDatagrid(
		"Group Members",
		"Members of this group",
		array(),
		array(
			'id' => 'ID',
			'data[GroupMember][UserName]' => 'Name',
			'data[GroupMember][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Group Attributes List Action
function processGroupAttributesList($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	$link .= '&do[]=attributes&id[]=' . urlencode($id);

	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/group_attributes/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/group_attributes/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[GroupAttribute][Name]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$output = $grid->drawDatagrid(
		"Group Attributes",
		"Attributes for the group",
		array(),
		array(
			'id' => 'ID',
			'data[GroupAttribute][Name]' => 'Name',
			'data[GroupAttribute][Operator]' => 'Operator',
			'data[GroupAttribute][Value]' => 'Value',
			'data[GroupAttribute][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Group Attributes Add Action
function processGroupAttributesAdd($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Link to submit back to
	$link .= '&do[]=attributes&id[]=' . urlencode($id);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/group_attributes/add/' . urlencode($id));
	$form = new AWIT_WHMCS_Html_Form();
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action . '/' . $do[0],
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action . '/' . $do[0]));

	// Processing form
	$output = $form->drawForm("Groups Attributes", "Create Group Attributes");

	if ($output !== false) {
		echo $output;
	}

}



// Group Attributes Edit Action
function processGroupAttributesEdit($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Link to submit back to, doPath type
	$link .= '&do[]=attributes&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	if (!empty($page)) {
		$entityManager->setReadQueryString('/group_attributes/index/' . urlencode($id[0]) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/group_attributes/index/' . urlencode($id[0]));
	}

	$entityManager->setWriteQueryString('/group_attributes/edit/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$form = new AWIT_WHMCS_Html_Form($id[1]);
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action . '/' . $do[0],
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action . '/' . $do[0]));

	// Processing form
	$output = $form->drawForm("Group Attributes", "Edit Group Attributes");

	if ($output !== false) {
		echo $output;
	}

}



// Group Attributes Delete Action
function processGroupAttributesDelete($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Handle delete subaction for attributes
	$link .= '&do[]=attributes&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/group_attributes/remove/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$data = $entityManager->writeData();
	if ($entityManager->errors($data) === false) {
		AWIT_WHMCS_Util::logSuccess('Successfully deleted Group Attribute');
	}

	$id = $id[0];
	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/group_attributes/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/group_attributes/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[GroupAttribute][Name]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$output = $grid->drawDatagrid(
		"Group Attributes",
		"Attributes for the group",
		array(),
		array(
			'id' => 'ID',
			'data[GroupAttribute][Name]' => 'Name',
			'data[GroupAttribute][Operator]' => 'Operator',
			'data[GroupAttribute][Value]' => 'Value',
			'data[GroupAttribute][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Groups List Action
function processGroupsList($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/groups/index?page=' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/groups/index');
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[Group][Name]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$grid->setCustomLocalActions(
		array(
			array(
				'title' => 'Group Attributes',
				'image' => 'images/icons/invoices.png',
				'link' => $link . '&do=attributes',
			),
			array(
				'title' => 'Group Members',
				'image' => 'images/icons/clients.png',
				'link' => $link . '&do=member',
			),
		)
	);

	$output = $grid->drawDatagrid(
		"Groups",
		"Radius group accounts",
		array(),
		array(
			'id' => 'ID',
			'data[Group][Name]' => 'Name',
			'data[Group][Priority]' => 'Priority',
			'data[Group][Disabled]' => 'Disabled',
			'data[Group][Comment]' => 'Comment',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}


// Group Add Action
function processGroupsAdd($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/groups/add');
	$form = new AWIT_WHMCS_Html_Form();
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action,
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action));

	// Processing form
	$output = $form->drawForm("Groups", "Create Group");

	if ($output !== false) {
		echo $output;
	}

}



// Group Edit Action
function processGroupsEdit($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Set the link to return to
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	if (!empty($page)) {
		$entityManager->setReadQueryString('/groups/index/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/groups/index');
	}

	$entityManager->setWriteQueryString('/groups/edit/' . urlencode($id));
	$form = new AWIT_WHMCS_Html_Form($id);
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action,
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action));

	// Processing form
	$output = $form->drawForm("Groups", "Edit Group");

	if ($output !== false) {
		echo $output;
	}

}



// User Topups List Action
function processUserTopupsList($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	$link .= '&do[]=topups&id[]=' . urlencode($id);

	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/user_topups/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/user_topups/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[UserTopup][Value]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$output = $grid->drawDatagrid(
		"User Topups",
		"Topups for user",
		array(),
		array(
			'id' => 'ID',
			'data[UserTopup][Type]' => 'Type',
			'data[UserTopup][Value]' => 'Value',
			'data[UserTopup][valid_from]' => 'Valid From',
			'data[UserTopup][valid_to]' => 'Valid To',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// User Topups Add Action
function processUserTopupsAdd($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Link to submit back to
	$link .= '&do[]=topups&id[]=' . urlencode($id);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/user_topups/add/' . urlencode($id));
	$form = new AWIT_WHMCS_Html_Form();
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action . '/' . $do[0],
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action . '/' . $do[0]));

	// Processing form
	$output = $form->drawForm("User Topups", "Create User Topup");

	if ($output !== false) {
		echo $output;
	}

}


// User Topups Edit Action
function processUserTopupsEdit($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Setting the queryString for write operations
	if (!empty($page)) {
		$entityManager->setReadQueryString('/user_topups/index/' . urlencode($id[0]) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/user_topups/index/' . urlencode($id[0]));
	}

	$entityManager->setWriteQueryString('/user_topups/edit/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$form = new AWIT_WHMCS_Html_Form($id[1]);
	$form->setEntityManager($entityManager);

	// Setting link to submit back to
	$link .= '&do[]=topups&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action . '/' . $do[0],
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action . '/' . $do[0]));

	// Processing form
	$output = $form->drawForm("User Topups", "Edit User Topups");

	if ($output !== false) {
		echo $output;
	}

}


// User Topups Delete Action
function processUserTopupsDelete($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	$link .= '&do[]=topups&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/user_topups/remove/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$data = $entityManager->writeData();
	if ($entityManager->errors($data) === false) {
		AWIT_WHMCS_Util::logSuccess('Successfully deleted User Topup');
	}

	$id = $id[0];
	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/user_topups/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/user_topups/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[UserTopup][Value]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$output = $grid->drawDatagrid(
		"User Topups",
		"Topups for user",
		array(),
		array(
			'id' => 'ID',
			'data[UserTopup][Type]' => 'Type',
			'data[UserTopup][Value]' => 'Value',
			'data[UserTopup][valid_from]' => 'Valid From',
			'data[UserTopup][valid_to]' => 'Valid To',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// User Groups List Action
function processUserGroupsList($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	$link .= '&do[]=user_groups&id[]=' . urlencode($id);

	// Handle pagination
	if (isset($page)) {
		$entityManager->setReadQueryString('/user_groups/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/user_groups/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[UserGroup][group]');
	$grid->setLocalActions('delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);

	$grid->setCustomMenuActions(
		array(
			array(
				'title' => 'Assign Group To User',
				'label' => 'Assign Group',
				'class' => 'btn-success',
				'link' => $link . '&do[]=add',
			),
		)
	);

	$output = $grid->drawDatagrid(
		"User Groups",
		"Groups assigned to the user",
		array(),
		array(
			'id' => 'ID',
			'data[UserGroup][group]' => 'Name',
			'data[UserGroup][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// User Groups Add Action
function processUserGroupsAdd($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Link to submit back to
	$link .= '&do[]=user_groups&id[]=' . urlencode($id);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/user_groups/add/' . urlencode($id));
	$form = new AWIT_WHMCS_Html_Form();
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action . '/' . $do[0],
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action . '/' . $do[0]));

	// Processing form
	$output = $form->drawForm("User Groups", "Assign User Groups");

	if ($output !== false) {
		echo $output;
	}

}



// User Groups Edit Action
function processUserGroupsEdit($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Setting the queryString for write operations
	if (isset($page)) {
		$entityManager->setReadQueryString('/user_groups/index/' . urlencode($id[0]) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/user_groups/index/' . urlencode($id[0]));
	}

	$entityManager->setWriteQueryString('/user_groups/edit/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$form = new AWIT_WHMCS_Html_Form($id[1]);
	$form->setEntityManager($entityManager);

	// Setting link to submit back to
	$link .= '&do[]=user_groups&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}
	// Setting the form action
	$form->setAction(
		array(
			'name' => $action . '/' . $do[0],
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action . '/' . $do[0]));

	// Processing form
	$output = $form->drawForm("User Groups", "Edit User Groups");

	if ($output !== false) {
		echo $output;
	}

}



// User Groups Delete Action
function processUserGroupsDelete($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Handle delete subaction for user_groups
	$link .= '&do[]=user_groups&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/user_groups/remove/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$data = $entityManager->writeData();
	if ($entityManager->errors($data) === false) {
		AWIT_WHMCS_Util::logSuccess('Successfully removed User from Group');
	}

	$id = $id[0];
	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/user_groups/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/user_groups/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[UserGroup][group]');
	$grid->setLocalActions('delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$output = $grid->drawDatagrid(
		"User Groups",
		"Radius groups assigned to the user",
		array(),
		array(
			'id' => 'ID',
			'data[UserGroup][group]' => 'Name',
			'data[UserGroup][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// User Attributes List Action
function processUserAttributesList($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	$link .= '&do[]=attributes&id[]=' . urlencode($id);

	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/user_attributes/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/user_attributes/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[UserAttribute][Name]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$output = $grid->drawDatagrid(
		"User Attributes",
		"Attributes for the user",
		array(),
		array(
			'id' => 'ID',
			'data[UserAttribute][Name]' => 'Name',
			'data[UserAttribute][Operator]' => 'Operator',
			'data[UserAttribute][Value]' => 'Value',
			'data[UserAttribute][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// User Attributes Add Action
function processUserAttributesAdd($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Link to submit back to
	$link .= '&do[]=attributes&id[]=' . urlencode($id);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/user_attributes/add/' . urlencode($id));
	$form = new AWIT_WHMCS_Html_Form();
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action . '/' . $do[0],
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action . '/' . $do[0]));

	// Processing form
	$output = $form->drawForm("Users Attributes", "Create User Attribute");

	if ($output !== false) {
		echo $output;
	}

}



// User Attributes Edit Action
function processUserAttributesEdit($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Setting link to submit back to, doPath type
	$link .= '&do[]=attributes&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	if (!empty($page)) {
		$entityManager->setReadQueryString('/user_attributes/index/' . urlencode($id[0]) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/user_attributes/index/' . urlencode($id[0]));
	}

	$entityManager->setWriteQueryString('/user_attributes/edit/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$form = new AWIT_WHMCS_Html_Form($id[1]);
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action . '/' . $do[0],
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action . '/' . $do[0]));

	// Processing form
	$output = $form->drawForm("User Attributes", "Edit User Attribute");

	if ($output !== false) {
		echo $output;
	}

}



// User Attributes Delete Action
function processUserAttributesDelete($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Handle delete subaction for attributes
	$link .= '&do[]=attributes&id[]=' . urlencode($id[0]);
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/user_attributes/remove/' . urlencode($id[1]) . '/' . urlencode($id[0]));
	$data = $entityManager->writeData();
	if ($entityManager->errors($data) === false) {
		AWIT_WHMCS_Util::logSuccess('Successfully deleted User Attribute');
	}

	$id = $id[0];
	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/user_attributes/index/' . urlencode($id) . '/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/user_attributes/index/' . urlencode($id));
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('name');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$output = $grid->drawDatagrid(
		"User Attributes",
		"Attributes for the user",
		array(),
		array(
			'id' => 'ID',
			'data[UserAttribute][Name]' => 'Name',
			'data[UserAttribute][Operator]' => 'Operator',
			'data[UserAttribute][Value]' => 'Value',
			'data[UserAttribute][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// User List Action
function processUsersList($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/users/indexall?page=' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/users/indexall');
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('data[User][Username]');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$grid->setCustomLocalActions(
		array(
			array(
				'title' => 'User Groups',
				'image' => 'images/icons/clients.png',
				'link' => $link . '&do=user_groups',
			),
			array(
				'title' => 'User Attributes',
				'image' => 'images/icons/invoices.png',
				'link' => $link . '&do=attributes',
			),
			array(
				'title' => 'User Topups',
				'image' => 'images/icons/income.png',
				'link' => $link . '&do=topups',
			),
		)
	);

	$output = $grid->drawDatagrid(
		"Users",
		"Radius user accounts",
		array(),
		array(
			'id' => 'ID',
			'data[User][Username]' => 'Username',
			'data[User][Password]' => 'Password',
			'data[User][Disabled]' => 'Disabled',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// User Add Action
function processUsersAdd($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/users/add');
	$form = new AWIT_WHMCS_Html_Form();
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action,
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action));

	// Processing form
	$output = $form->drawForm("Users", "Create User");

	if ($output !== false) {
		echo $output;
	}

}



// User Edit Action
function processUsersEdit($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Set the link to return to
	if (!empty($page)) {
		$link .= '&page=' . urlencode($page);
	}

	// Setting the queryString for write operations
	if (isset($page)) {
		$entityManager->setReadQueryString('/users/indexall/page:' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/users/indexall');
	}

	$entityManager->setWriteQueryString('/users/edit/' . urlencode($id));
	$form = new AWIT_WHMCS_Html_Form($id);
	$form->setEntityManager($entityManager);

	// Setting the form action
	$form->setAction(
		array(
			'name' => $action,
			'method' => 'POST',
			'action' => $link,
		)
	);

	// Adding elements from configuration
	$form->setElements($formsManager->getForm($action));

	// Processing form
	$output = $form->drawForm("Users", "Edit User");

	if ($output !== false) {
		echo $output;
	}

}



// User Delete Action
function processUsersDelete($link, $doPath, $do, &$entityManager, $id, $action, $formsManager, $page)
{
	// Setting the queryString for write operations
	$entityManager->setWriteQueryString('/users/remove/' . urlencode($id));
	$data = $entityManager->writeData();
	if ($entityManager->errors($data) === false) {
		AWIT_WHMCS_Util::logSuccess('Successfully deleted User');
	}

	// Handle pagination
	if (!empty($page)) {
		$entityManager->setReadQueryString('/users/indexall?page=' . urlencode($page));
	} else {
		$entityManager->setReadQueryString('/users/indexall');
	}

	// Display list of items
	$grid = new AWIT_WHMCS_Html_Datagrid();
	$grid->setEntityManager($entityManager);
	$grid->setShowCheckbox(true);
	$grid->setLabel('default');
	$grid->setShowClientSelect(false);
	$grid->setValidation(array());
	$grid->setLink($link);
	$grid->setLabelKey('name');
	$grid->setLocalActions('edit|delete');
	$grid->setGlobalActions('delete');
	$grid->setShowSearch(true);
	$grid->setShowPagination(true);
	$grid->setMenuActions('add');

	$grid->setCustomLocalActions(
		array(
			array(
				'title' => 'Groups',
				'image' => 'images/icons/clients.png',
				'link' => $link . '&do=groups',
			),
			array(
				'title' => 'Attributes',
				'image' => 'images/icons/invoices.png',
				'link' => $link . '&do=attributes',
			),
		)
	);

	$output = $grid->drawDatagrid(
		"Users",
		"Radius user accounts",
		array(),
		array(
			'id' => 'ID',
			'data[User][Name]' => 'Name',
			'data[User][Username]' => 'Username',
			'data[User][Password]' => 'Password',
		)
	);

	if ($output !== false) {
		echo $output;
	}

}



// Filtering array for action and do parameters
function filter_array($val) {
	return preg_replace('/[^A-Z_-]/i', '', $val);
}



// Filtering array for action and do parameters
function array_to_int($val) {
	return intval($val);
}



// Addon output
function awit_smradius_output($vars)
{
	global $whmcsConfig;
	global $entityManager;
	global $id;
	global $action;
	global $formsManager;

	// Make link to use
	$link = htmlentities($vars['modulelink']);

	// Get action
	$action = "mg-users";
	if (!empty($_GET['action'])) {
		$action = preg_replace('/[^A-Z_]/i', '', $_GET['action']);
	}

	// Get pagination at different depths
	$page = "";
	if (isset($_REQUEST['page'])) {
		$page = intval($_REQUEST['page']);
	}

	// Get do at different depths
	$do = "";
	$doPath = "";
	if (!empty($_GET['do'])) {
		$do = $_GET['do'];
		$doDepth = 1;
		if (is_array($do) && count($do) == 1) {
			$do = preg_replace('/[^A-Z_]/i', '', $do[0]);
		} else if (is_array($do)) {
			$do = array_filter($do, "filter_array");
			$doPath = implode('/', $do);
		}
	}

	// Get id at different depths
	$id = "";
	$idPath = "";
	if (!empty($_GET['id'])) {
		$id = $_GET['id'];
		if (is_array($id) && count($id) == 1) {
			$id = intval($id[0]);
		} else if (is_array($id)) {
			$id = array_filter($id, "arr_to_int");
			$idPath = implode('/', $id);
		}
	}

	$searchQuery = "";
	if (!empty($_REQUEST['q'])) {
		$searchQuery = htmlentities($_REQUEST['q']);
	}

	// Define Management Actions
	$managementActions = array (
		'mg-users' => 'Users',
		'mg-groups' => 'Groups',
		'mg-realms' => 'Realms',
		'mg-clients' => 'Clients',
		'mg-hotspots' => 'Hotspots',
		'mg-nas' => 'Nas',
		'mg-usergroups' => 'User-Groups',
		'mg-profiles' => 'Profiles',
		'mg-attributes' => 'Attributes',
		'mg-realms' => 'Realms',
		'mg-ippools' => 'IP-Pools',
	);

	// Define Billing Actions
	$billingActions = array (
		'bill-pos' => 'POS',
		'bill-plans' => 'Plans',
		'bill-rates' => 'Rates',
	);

	// Define Settings Actions
	$settingActions = array (
		'st-servers' => 'Servers',
		'st-license' => 'License Info',
	);

	// Grouping Menu Conditions
	$isManagement = $action == "" || $action == "management" || in_array($action, array_keys($managementActions));
	$isBilling = $action == "billing" || in_array($action, array_keys($billingActions));
	$isSettings = $action == "settings" || in_array($action, array_keys($settingActions));

	// Main Menu
	echo "<div id='tabs' class='tabs'>";
	echo "<ul>";
	echo "<li " . ($isManagement ? "class='tabselected'" : "") .
			"><a href='".htmlentities($link)."'> Management </a></li>";
	echo "<li " . ($isBilling ? "class='tabselected'" : "") .
			"><a href='".htmlentities($link)."&action=bill-pos'> Billing </a></li>";
	echo "<li " . ($isSettings ? "class='tabselected'" : "") .
			"><a href='".htmlentities($link)."&action=st-servers'> Settings </a></li>";
	echo "</ul>";
	echo "</div>";
	echo "<br/>";

	// Processing form submissions if any
	if (isset($_REQUEST['AWIT_WHMCS_Html_Form_Key']) && !empty($_REQUEST['AWIT_WHMCS_Html_Form_Key'])) {
		$formKey = $_REQUEST['AWIT_WHMCS_Html_Form_Key'];
		$formsManager->processForm($formKey);
	}

	if ($isManagement) {
		// Management Menu Screen

		echo "<div id='tabs' class='tabs'>";
		echo "<ul>";
		foreach ($managementActions as $key => $val) {
			echo "<li " . ($action == $key ? "class='tabselected'" : "") .
					"><a href='".htmlentities($link)."&action=".htmlentities($key)."'> ".$val." </a></li>";
		}
		echo "</ul>";
		echo "</div>";
		echo "<br/>";

		// Clients Screen
		if ($action == "mg-clients") {
			$link .= '&action=mg-clients';


			//  Client Realms
			// Setting the prefix for unique JSON response identification
			$entityManager->setPrefixData('ClientRealms');
			if ($doPath == 'realm/delete') {
				processClientRealmsDelete($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($doPath == 'realm/add') {
				processClientRealmsAdd($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'realm') {
				processClientRealmsList($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			}

			//  Client Attributes
			$entityManager->setPrefixData('ClientAttributes');
			// Handle edit subaction for attributes
			if ($doPath == 'attributes/edit') {
				processClientAttributesEdit($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($doPath == 'attributes/delete') {
				processClientAttributesDelete($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($doPath == 'attributes/add') {
				processClientAttributesAdd($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'attributes') {
				processClientAttributesList($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			}

			// Clients
			$entityManager->setPrefixData('Clients');
			if ($do == 'add') {
				processClientsAdd($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'delete') {
				processClientsDelete($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'edit') {
				processClientsEdit($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else {
				processClientsList($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			}
		}

		// Realms Screen
		if ($action == "mg-realms") {
			$link .= '&action=mg-realms';

			//  Realm Members
			// Setting the prefix for unique JSON response identification
			$entityManager->setPrefixData('RealmMembers');
			if ($doPath == 'member/delete') {
				processRealmMembersDelete($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'member') {
				processRealmMembersList($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			}

			//  Realm Attributes
			$entityManager->setPrefixData('RealmAttributes');
			// Handle edit subaction for attributes
			if ($doPath == 'attributes/edit') {
				processRealmAttributesEdit($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($doPath == 'attributes/delete') {
				processRealmAttributesDelete($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($doPath == 'attributes/add') {
				processRealmAttributesAdd($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'attributes') {
				processRealmAttributesList($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			}

			// Realms
			$entityManager->setPrefixData('Realms');
			if ($do == 'add') {
				processRealmsAdd($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'delete') {
				processRealmsDelete($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'edit') {
				processRealmsEdit($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else {
				processRealmsList($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			}
		}

		// Groups Screen
		if ($action == "mg-groups") {
			$link .= '&action=mg-groups';

			// Set the table
			$entityManager->setTable('tbl_awit_smradius_groups');

			// Group Members
			// Setting the prefix for unique JSON response identification
			$entityManager->setPrefixData('GroupMembers');
			if ($doPath == 'member/delete') {
				processGroupMembersDelete($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'member') {
				processGroupMembersList($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			}

			//  Group Attributes
			$entityManager->setPrefixData('GroupAttributes');
			// Handle edit subaction for attributes
			if ($doPath == 'attributes/edit') {
				processGroupAttributesEdit($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($doPath == 'attributes/delete') {
				processGroupAttributesDelete($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($doPath == 'attributes/add') {
				processGroupAttributesAdd($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'attributes') {
				processGroupAttributesList($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			}

			// Groups
			$entityManager->setPrefixData('Groups');
			if ($do == 'add') {
				processGroupsAdd($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'delete') {
				processGroupsDelete($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'edit') {
				processGroupsEdit($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else {
				processGroupsList($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			}
		}

		// Users Screen
		if ($action == "mg-users") {
			$link .= '&action=mg-users';

			//  User Topups
			$entityManager->setPrefixData('UserTopups');
			// Handle edit subaction for topups
			if ($doPath == 'topups/edit') {
				processUserTopupsEdit($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($doPath == 'topups/delete') {
				processUserTopupsDelete($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($doPath == 'topups/add') {
				processUserTopupsAdd($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'topups') {
				processUserTopupsList($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			}

			//  User Groups
			$entityManager->setPrefixData('UserGroup');
			// Handle edit subaction for user_groups
			if ($doPath == 'user_groups/edit') {
				processUserGroupsEdit($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($doPath == 'user_groups/delete') {
				processUserGroupsDelete($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($doPath == 'user_groups/add') {
				processUserGroupsAdd($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'user_groups') {
				processUserGroupsList($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			}

			//  User Attributes
			$entityManager->setPrefixData('UserAttributes');
			// Handle edit subaction for attributes
			if ($doPath == 'attributes/edit') {
				processUserAttributesEdit($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($doPath == 'attributes/delete') {
				processUserAttributesDelete($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($doPath == 'attributes/add') {
				processUserAttributesAdd($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'attributes') {
				processUserAttributesList($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			}

			// Users
			$entityManager->setPrefixData('Users');
			if ($do == 'add') {
				processUsersAdd($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'delete') {
				processUsersDelete($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else if ($do == 'edit') {
				processUsersEdit($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			} else {
				processUsersList($link, $doPath, $do, $entityManager, $id, $action, $formsManager, $page);
				return;
			}
		}

	} else if ($isBilling) {
		// Billing Menu Screen
		echo "<div id='tabs' class='tabs'>";
		echo "<ul>";
		foreach ($billingActions as $key => $val) {
			echo "<li " . ($action == $key ? "class='tabselected'" : "") .
					"><a href='".htmlentities($link)."&action=".htmlentities($key)."'> ".$val." </a></li>";
		}
		echo "</ul>";
		echo "</div>";
		echo "<br/>";

	} else if ($isSettings) {
		// Settings Menu Screen
		echo "<div id='tabs' class='tabs'>";
		echo "<ul>";
		foreach ($settingActions as $key => $val) {
			echo "<li " . ($action == $key ? "class='tabselected'" : "") .
					"><a href='".htmlentities($link)."&action=".htmlentities($key)."'> ".$val." </a></li>";
		}
		echo "</ul>";
		echo "</div>";
		echo "<br/>";

	}
}
