<?php

$_ADDONLANG['intro'] = "Module to Facilitate SMRadius Integration";
$_ADDONLANG['description'] = <<<EOF
This module replicates portions of the SMRadius Web User Interface to WHMCS
EOF;

$_ADDONLANG['documentation'] = <<<EOF
See https://gitlab.devlabs.linuxassist.net/awit-whmcs/awit-whmcs-smradius/wikis/home
EOF;

?>
