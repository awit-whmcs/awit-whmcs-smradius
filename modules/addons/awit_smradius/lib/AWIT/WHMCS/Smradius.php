<?php
/**
 * AWIT SMRADIUS - Bank Transaction Processing Class
 * Copyright (c) 2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we being used from within whmcs framework for functions like logModuleCall etc.
if (!defined("WHMCS")) {
	die("This file cannot be accessed directly");
}


/**
 * @class AWIT_WHMCS_Smradius
 */
class AWIT_WHMCS_Smradius {



	/**
	 * @var $AWIT_SMRADIUS_SUPPORTED_FIELDS
	 * Our global supported field list
	 */
	private $AWIT_SMRADIUS_SUPPORTED_FIELDS = array(
		"smradius_url", "smradius_cookie_file", "smradius_username", "smradius_password"
	);



	/**
	 * @method getConfigArray
	 * Retrieve all custom fields and values pertaining to this module
	 * Returns an array of config variables
	 * @return $customFields - Configuration Array
	 */
	public function getConfigArray()
	{
		// Query modules table
		$table = "tbladdonmodules";
		$fields = "setting,value";
		$where = array( 'module' => "awit_smradius" );
		$result = select_query($table,$fields,$where);

		// Filter out the settings we need
		$customFields = array();
		while ($row = mysql_fetch_array($result)) {
			// Check in our global list
			if (in_array($row['setting'],$this->AWIT_SMRADIUS_SUPPORTED_FIELDS)) {
				$customFields[$row['setting']] = $row['value'];
			}
		}

		return $customFields;
	}



	/**
	 * @method getConfig
	 * Gets the config var @ the specified key
	 * @return Returns the value of the specified config or false if not found
	 */
	public function getConfig($key) {
		$customFields = $this->getConfigCustomFields();
		if (isset($customFields[$key])) {
			return $customFields[$key];
		} else {
			return false;
		}
	}



}

