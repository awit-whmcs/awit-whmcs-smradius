<?php
/**
 * AWIT SMRADIUS - SMRadius Rest Client
 * Copyright (c) 2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we being used from within whmcs framework for functions like logModuleCall etc.
if (!defined("WHMCS")) {
	die("This file cannot be accessed directly");
}



/**
 * @class AWIT_WHMCS_Data_Drivers_Mysql
 * @brief The Mysql Driver for use with the AWIT_WHMCS_Data_Connection class
 *
 * This class exposes methods needed to interact with Mysql databases
 * and provides data formatted to work with the display classes
 */
class AWIT_WHMCS_Data_Drivers_CakePhpRest extends AWIT_WHMCS_Data_Connection
{



	/**
	 * @var $config
	 * The configuration for performing operations on the database
	 *
	 * ```
	 * Setting up configuration to connect to alternate host
	 * $config['url'] = 'http://cakephphost/';
	 * $config['user'] = 'root';
	 * $config['password'] = 'password';
	 * $config['ssl'] = 'on';
	 * ```
	 *
	 */
	private $config;



	/**
	 * @var $schema
	 * The schema against which to perform database operations
	 *
	 * ```
	 * Import constraints to check for duplicates when inserting
	 * $schema['uniquekey'] = array('field1', 'field2', 'field3')
	 *
	 * Specifying table
	 * $schema['awit_tbl_tablename'] = array (
	 *		'fields' => array('field1', 'field2')
	 * );
	 *
	 * Specifying fields (Must conform to valid field names for Mysql)
	 * $schema['fields'] = array('field1', 'field2');
	 *
	 * ```
	 *
	 */
	private $schema;



	/**
	 * @var $prefixData
	 * The value to prefix data with, for use with identifying data from
	 * different locations with the same structure when mapping schemas
	 */
	private $prefixData;



	/**
	 * @var $queryString
	 * The query string against which to perform rest operations
	 *
	 */
	private $queryString;



	/**
	 * @var $loginAttempts
	 * The amount of times a login attempt was made
	 *
	 */
	private $loginAttempts;



	/**
	 * @var $loggedIn
	 * Stores weather or not a login was successfull
	 *
	 */
	private $loggedIn;



	/**
	 * @var $httpCode
	 * Stores httpCode for the previous request
	 *
	 */
	private $httpCode;



	/**
	 * @var $httpHeaders
	 * Stores httpHeaders for the previous request
	 *
	 */
	private $httpHeaders;



	/**
	 * @var $cookie
	 * The cookie stored internally for use with authenticated requests
	 *
	 */
	private $cookie;



	/**
	 * @var $path
	 * The result path stored internally
	 *
	 */
	private $path;



	/**
	 * @var $currentPath
	 * The current path stored internally
	 *
	 */
	private $currentPath;



	/**
	 * @var $currentSchema
	 * The currentSchema matched for the current record
	 *
	 */
	private $currentSchema;



	/**
	 * @var $formattedItem
	 * The list of formatted data records
	 *
	 */
	private $formattedItem;



	/**
	 * @var $cookie
	 * The cookie stored internally for use with authenticated requests
	 *
	 */
	private $formattedData;



	/**
	 * @method __construct
	 * Constructs the AWIT_WHMCS_Data_Drivers_CakePhpRest object
	 */
	public function __construct($config = array(), $schema = array())
	{
		$this->config = $config;
		$this->loggedIn = false;
		$this->loginAttempts = 0;
		$this->httpHeaders = array();
		$this->httpCode = 0;

		if (!empty($schema)) {
			$this->schema = new AWIT_WHMCS_Data_Schema($schema);
		}
	}



	/**
	 * @method setResource
	 * Sets the resourceId for the driver
	 *
	 * @return (bool) True / False
	 */
	public function setResource($resource)
	{
		$this->setQueryString($resource);
	}



	/**
	 * @method getResource
	 * Gets the resource for the driver
	 *
	 * @return (bool) True / False
	 */
	public function getResource()
	{
		return $this->getQueryString($resource);
	}



	/**
	 * @method getProvider
	 * gets the provider
	 */
	public function getProvider()
	{
		if (!is_object($this->provider)) {
			$class = get_class($this);
			$this->provider = new $class($this->config);
		}

		return $this->provider;
	}



	/**
	 * @method getConfig
	 * gets the config
	 */
	public function getConfig()
	{
		return new AWIT_WHMCS_Data_Config($this->config);
	}



	/**
	 * @method setConfig
	 * sets the config
	 */
	public function setConfig($config)
	{
		$this->config = $config;
	}



	/**
	 * @method getSchema
	 * gets the schema
	 */
	public function getSchema()
	{
		if (is_object($this->schema) && get_class($this->schema) == 'AWIT_WHMCS_Data_Schema') {
			return $this->schema;
		} else if (is_array($this->schema)) {
			return new AWIT_WHMCS_Data_Schema($this->schema);
		}

		return array();
	}



	/**
	 * @method setSchema
	 * sets the schema
	 */
	public function setSchema($schema)
	{
		$this->schema = new AWIT_WHMCS_Data_Schema($schema);
		$this->provider = $this->getProvider();
	}



	/**
	 * @method getReadQueryString
	 * Gets the read query string
	 */
	public function getReadQueryString()
	{
		$schema = $this->getSchema()->getSchema();
		if (isset($schema['read-query-string'])) {
			$this->readQueryString = $schema['read-query-string'];
		} else {
			throw new Exception('AWIT_WHMCS_DATA_Driver_CakePHPRest:: Read Query String not set');
		}

		return $this->readQueryString;
	}



	/**
	 * @method setReadQueryString
	 * Sets the read query string
	 */
	public function setReadQueryString($readQueryString)
	{
		$this->getSchema()->setSchemaPart(array('read-query-string' => $readQueryString));
		$this->readQueryString = $readQueryString;
	}



	/**
	 * @method getWriteQueryString
	 * Gets the writeQueryString
	 */
	public function getWriteQueryString()
	{
		$schema = $this->getSchema()->getSchema();
		if (isset($schema['write-query-string'])) {
			$this->writeQueryString = $schema['write-query-string'];
		} else {
			throw new Exception('AWIT_WHMCS_DATA_Driver_CakePHPRest:: Write Query String not set');
		}

		return $this->writeQueryString;
	}



	/**
	 * @method setWriteQueryString
	 * Sets the query string
	 */
	public function setWriteQueryString($writeQueryString)
	{
		$this->getSchema()->setSchemaPart(array('write-query-string' => $writeQueryString));
		$this->writeQueryString = $writeQueryString;
	}



	/**
	 * @method setPrefixData
	 * Sets the prefixDataId for the driver
	 *
	 * @return (bool) True / False
	 */
	public function setPrefixData($prefixData)
	{
		$this->prefixData = $prefixData;
	}



	/**
	 * @method getPrefixData
	 * Gets the prefixData for the driver
	 *
	 * @return (bool) True / False
	 */
	public function getPrefixData()
	{
		return $this->prefixData;
	}



	/**
	 * @method _setHeader
	 * Callback to populates the headers for the currently returning curl response
	 *
	 * @param $ch The current handle
	 * @param $header The current header
	 * @return Returns the lenth of the current header
	 */
	private function _setHeader($ch, $header) {
		array_push($this->httpHeaders, $header);
		return strlen($header);
	}



	/**
	 * @method _login
	 * Performs authentication against the configured 'auth' details
	 *
	 * @return (bool) $loggedIn to indicate if login was successfull
	 */
	private function _login() {
		// Checking for auth details from config and perform login if not already logged in
		$config = $this->getConfig()->find('auth');
		if (!empty($config) && $this->loggedIn !== true) {
			// Authenticating
			$key = end(array_keys($config['username']));
			$value = end(array_values($config['username']));
			if (empty($key) || empty($value)) {
				throw new Exception('AWIT_WHMCS_Driver_SMRADIUS_CAKEPHP_Rest:: Config username not set');
			}

			$key = end(array_keys($config['password']));
			$value = end(array_values($config['password']));
			if (empty($key) || empty($value)) {
				throw new Exception('AWIT_WHMCS_Driver_SMRADIUS_CAKEPHP_Rest:: Config password not set');
			}

			if (empty($config['login-query-string'])) {
				throw new Exception('AWIT_WHMCS_Driver_SMRADIUS_CAKEPHP_Rest:: Login query string not set');
			}

			if (empty($config['session-user-id'])) {
				throw new Exception('AWIT_WHMCS_Driver_SMRADIUS_CAKEPHP_Rest:: Session User ID not set');
			}

			$keyUsername = end(array_keys($config['username']));
			$keyPassword = end(array_keys($config['password']));

			$data = array (
				$keyUsername => $config['username'][$keyUsername],
				$keyPassword => $config['password'][$keyPassword]
			);

			$url = $this->config['url'];
			$url .= $config['login-query-string'];

			$this->cookie = '';

			$result = $this->curlRequest($url, $data,
				array(
					'AWIT_PARSE_JSON' => false,
				)
			);

			if (intval($this->httpCode) >= 200 && intval($this->httpCode) < 300) {
				$this->loggedIn = true;
			} else {
				$this->loggedIn = false;
			}

		}

		return $this->loggedIn;
	}



	/**
	 * @method readData
	 * Reads data from the specified data source
	 * Will read all data unless matches are specified
	 *
	 * @param $matches array of field/value pairs to match by
	 * @return $data An array of key/value paired records
	 */
	public function readData($matches = array())
	{
		// Checking for valid query string
		if (empty($this->readQueryString)) {
			throw new Exception('AWIT_WHMCS_Driver_SMRADIUS_CAKEPHP_Rest:: Read Query String not set');
		}

		// Perform login
		$this->_login();

		// Resetting httpHeaders
		$this->httpHeaders = array();

		$url = $this->config['url'] . $this->readQueryString;
		$data = $this->curlRequest($url, null,
			array(
				'AWIT_PARSE_JSON' => false,
				'CURLOPT_POST' => 0,
				'CURLOPT_CUSTOMREQUEST' => 'GET',
				'CURLOPT_HTTPGET' => true,
			)
		);

		// Trying to parse JSON
		$data = json_decode($data, true);

		// Validate JSON
		if (is_null($data)) {
			throw new Exception('AWIT_WHMCS_Driver_SMRADIUS_CAKEPHP_Rest:: Invalid JSON returned with message ['.
					AWIT_WHMCS_Util::json_last_error_msg().'] from URL ['.$url.']');
		}

		// Checking AWIT JSON Data Structure
		if (!isset($data['status'])) {
			throw new Exception('AWIT_WHMCS_Driver_SMRADIUS_CAKEPHP_Rest:: Bad data structure returned from URL :['.$url.']');
		}

		// Throw exception on fail
		if ($data['status'] === 'fail') {
			throw new Exception('AWIT_WHMCS_Driver_SMRADIUS_CAKEPHP_Rest: Read Data Failed [' . $data['message'] . ']');
		} else if ($data['status'] !== 'success') {
			throw new Exception(
				'AWIT_WHMCS_Driver_SMRADIUS_CAKEPHP_Rest: Unknown Status Returned From Read Data [' . $data['status'] . ']'
			);
			return false;
		}

		$data = $data['data'];

		if (empty($data)) {
			AWIT_WHMCS_Util::logInfo("No Data", 'No data returned');
			return array();
		}

		// Setting prefix to handle multiple data types of data with the same response
		if (!empty($this->prefixData)) {
			$data = array (
				$this->prefixData => $data
			);
		}

		// Find simple defined arrays
		$schema = $this->getSchemaByData($data, 'array');
		if (!empty($schema)) {
			// Trail path for array to return
			$arrayPaths = explode('/', $schema['array']);
			$arrayPointer = $data;
			foreach ($arrayPaths as $path) {
				$arrayPointer = &$arrayPointer[$path];
			}
			return $arrayPointer;
		} else {
			// Find records/objects
			// Find the first matching schema definition for data by searching for 'field' keys
			$schema = $this->getSchemaByData($data, 'fields');

			if (empty($schema)) {
				// Check for empty data
				// Find the first matching schema definition for data by searching for 'empty-message' keys
				$schemaNoData = $this->getSchemaByData($data, 'no-data-message');
				if (!empty($schemaNoData)) {
					AWIT_WHMCS_Util::logInfo("No Data", $schemaNoData['no-data-message']);
					return array();
				}

				throw new Exception('AWIT_WHMCS_Driver_SMRADIUS_CAKEPHP_Rest:: Missing schema mapping from URL ['.$url.'] [' .
						str_replace("\n", "<br/>\n", var_export($schema, true) . ']'));
			}
		}


		// Massage data to schema mappings
		$data = $this->formatData($data);

		// Return all data
		if (empty($matches)) {
			return $data;
		} else {
			$resData = array();
			// Return on matches
			foreach ($matches as $matchesKey => $matchesValue) {
				foreach ($data as $dataKey => $dataValue) {
					foreach ($dataValue as $itemKey => $itemValue) {
						// If item matches on conditions add row
						if ($matchesKey === $itemKey && $itemValue == $matchesValue) {
							array_push($resData, $dataValue);
							break;
						}
					}
				}
			}

			return $resData;
		}
	}



	/**
	 * @method readRawData
	 * Reads data from the specified data source
	 * Will read all data unless matches are specified
	 *
	 * @param $matches array of field/value pairs to match by
	 * @return $data An array of key/value paired records
	 */
	public function readRawData($matches = array())
	{
		// Checking for valid query string
		if (empty($this->readQueryString)) {
			throw new Exception('AWIT_WHMCS_Driver_SMRADIUS_CAKEPHP_Rest:: Read Query String not set');
		}

		$this->_login();

		// Resetting httpHeaders
		$this->httpHeaders = array();

		$url = $this->config['url'] . $this->readQueryString;
		$data = $this->curlRequest($url, $data);

		// Setting prefix to handle multiple data types of data with the same response
		if (!empty($this->prefixData)) {
			$data = array (
				$this->prefixData => $data
			);
		}

		return $data;
	}



	/**
	 * @method formatData
	 * This method recursively loops over all the items in the data
	 * and returns an array of schema formatted records
	 *
	 * @param $inputArrayPart the array to format
	 * @return $this->formattedData An array formatted records
	 */
	public function formatData($inputArrayPart = array()) {
		// Set formattedData
		if (empty($this->formattedData)) {
			$this->formattedData = array();
		}

		// Setting index path to depth for recursion
		if (empty($this->path)) {
			$this->path = array();
		}

		// Set tmp array
		if (!empty($inputArrayPart)) {
			$inputArray = $inputArrayPart;
		}

		if (!is_array($inputArray)) {
			return false;
		}

		// Looping through inputArray to extract paths
		foreach ($inputArray as $k => $v) {
			if (!is_array($v)) {
				// Add path to collected array
				// Check for associative values over integer
				if (is_string($k)) {
					$pathKey = $k;
				} else {
					$pathKey = $v;
				}

				array_push($this->path, $pathKey);

				$path = implode('/', $this->path);

				$schemaTable = $this->schema->findAll('fields');
				$schemaPaths = $this->schema->getPath();
				$this->schema->resetPath();

				// Search against schema def for valid match by regex path
				$this->currentPath = '';
				$schemaPath = '';
				foreach ($schemaPaths as $sPath) {
					$sPath = str_replace("/fields", "", $sPath);
					if (@preg_match($sPath, $path)) {
						$this->currentPath = $sPath;
						$schemaPath = $sPath;
						$schema = $this->schema->find($sPath);
					}
				}

				// Caching the current schema
				$this->currentSchema = $schema;

				// If schema mapping found, format data
				if (!empty($schema)) {
					// Loop through response items fields to sniff out key matching values
					$retItem = array();
					foreach ($schema['fields'] as $fieldKey => $fieldValue) {
						$dataField = end(explode('/', $path));
						if (isset($schema['fields'][$dataField])) {
							$this->formattedItem[$schemaPath][$schema['fields'][$dataField]] = htmlentities($v);
						}
					}

					if (!empty($retItem)) {
						array_push($resData, $retItem);
					}
				}

				// Pop off the last element
				array_pop($this->path);
			}

			// Searching for matches in child nodes
			if (is_array($v) && !empty($v)) {
				array_push($this->path, $k);
				$this->formatData($v);
			}
		}

		// Filling non populated data
		foreach ($this->currentSchema['fields'] as $fieldKey => $fieldValue) {
			if (!isset($this->formattedItem[$this->currentPath][$fieldValue])) {
				$this->formattedItem[$this->currentPath][$fieldValue] = '';
			}
		}

		// Preventing duplicates as a result of recursion
		if (@preg_match($this->currentPath, implode('/', $this->path))) {
			// Collect the extracted item
			array_push($this->formattedData, $this->formattedItem[$this->currentPath]);
			unset($this->formattedItem[$this->currentPath]);
		}

		// Pop off the last element
		array_pop($this->path);

		return $this->formattedData;
	}



	/**
	 * @method getSchemaByData
	 * Gets the first matching schema for any of the data records
	 *
	 * @param $data array of data
	 * @return $schema or false if none found
	 */
	public function getSchemaByData($data, $search = 'fields') {
		// Find all paths to "fields" setting in schema
		$schemaTable = $this->schema->findAll($search);
		$schemaPaths = $this->schema->getPath();
		$this->schema->resetPath();

		// Find all paths in returned data
		$dataPaths = $this->schema->findPaths($data);

		// Search against schema def for valid match by regex breadcrumb
		foreach ($schemaPaths as $sPath) {
			foreach ($dataPaths as $dPath) {
				$sPath = str_replace("/" . $search, "", $sPath);
				if (@preg_match($sPath, $dPath)) {
					return $this->schema->find($sPath);
				}
			}
		}

		return false;
	}



	/**
	 * @method writeData
	 * Writes data to the specified data source
	 * Checks for duplicates if specified
	 *
	 * @param $data array of records in the form
	 *        array (
	 *            array('field' => 'value'),
	 *            array('field' => 'value')
	 *        )
	 *        OR just a single record
	 *        array('field' => 'value')
	 *
	 * @param $matches Where updates field/value pairs can be specified
	 *        Only records matching these will be updated
	 * @return Returns array('imported', 'duplicates')
	 * @throws Throws exceptions on any mysql errors
	 */
	public function writeData($data, $matches = array()) {
		// Checking for valid query string
		if (empty($this->writeQueryString)) {
			throw new Exception('AWIT_WHMCS_Driver_SMRADIUS_CAKEPHP_Rest:: Write Query String not set');
		}

		// Perform login
		$this->_login();

		// Resetting httpHeaders
		$this->httpHeaders = array();

		$url = $this->config['url'] . $this->writeQueryString;

		$data = $this->curlRequest($url, $data);

		// Checking AWIT JSON Data Structure
		if (!isset($data['status'])) {
			throw new Exception('AWIT_WHMCS_Driver_SMRADIUS_CAKEPHP_Rest:: Bad data structure returned');
		}

		// Catching data
		if (isset($data['data'])) {
			$data = $data['data'];
		}

		// Setting prefix to handle multiple data types of data with the same response
		if (!empty($this->prefixData)) {
			$data = array (
				$this->prefixData => $data
			);
		}


		return $data;
	}



	/**
	 * @method curlRequest
	 * Performs a CURL request to the specified url with the specified curl options
	 * which override the default options
	 *
	 * @param $url URL to send request to
	 * @param $data Array of data to send
	 * @param $options Combination of custom and CURL_OPT options
	 *		- ['AWIT_PARSE_JSON'] = true; To parse JSON and return the resulting array
	 * @return Returns array('imported', 'duplicates')
	 * @throws Throws exceptions on any mysql errors
	 */
	public function curlRequest($url, $data, $options = array()) {
		// Checking for valid query string
		if (empty($url)) {
			AWIT_WHMCS_Util::debug(sprintf('curlRequest, url empty, %s, data, %s, curlOptions, %s, ',
				$url,
				print_r($data, true),
				print_r($curlOptions, true)
			));
			return false;
		}

		// Setting default options
		if (!isset($options['AWIT_PARSE_JSON'])) {
			$options['AWIT_PARSE_JSON'] = true;
		}

		// Resetting httpHeaders
		$this->httpHeaders = array();

		$ch = curl_init($url);

		// Setting to POST if post data was specified
		if (!empty($data)) {
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		}

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if (!empty($this->config['ssl-cert'])) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
			curl_setopt($ch, CURLOPT_CAINFO, $this->config['ssl-cert']);
		} else {
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		}

		curl_setopt($ch, CURLOPT_HTTPHEADER,
			array(
				'Accept: application/json'
			)
		);
		curl_setopt($ch, CURLOPT_HEADERFUNCTION, array ($this, '_setHeader'));

		// Setting timeout options
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);

		// Using cookie if any set
		if (!empty($this->config['auth']) && !empty($this->cookie)) {
			curl_setopt($ch, CURLOPT_COOKIE, $this->cookie);
		}

		// Returning header info to extract cookie
		if (!empty($this->config['auth'])) {
			curl_setopt($ch, CURLOPT_HEADER, 0);
		}

		// Strip out CURL options
		$curlOptions = array();
		foreach ($options as $key => $value) {
			if (strpos($key, 'CURL') > -1) {
				$curlOptions[$key] = $value;
			}
		}

		// Overriding with curlOptions if any
		if (!empty($curlOptions)) {
			curl_setopt_array($ch, $curlOptions);
		}

		$result = curl_exec($ch);

		// Catching HTTP Code
		$this->httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		$isError = $this->httpErrors($this->httpCode);
		if ($isError === true) {
			return $isError;
		}

		// Parse data for cookie header, store cookie as it will be used for subsequent requests
		if (!empty($this->config['auth'])) {
			foreach ($this->httpHeaders as $header) {
				$pattern = '/Set-Cookie:(.*)/i';
				if (preg_match($pattern, $header, $output)) {
					$this->cookie = $output[1];
				}
			}
		}

		// If AWIT_PARSE_JSON then return the raw response
		if ($options['AWIT_PARSE_JSON'] !== true) {
			return $result;
		}

		// Decode json to associative array
		$data = json_decode($result, true);
		if (is_null($data)) {
			throw new Exception('AWIT_WHMCS_Driver_SMRADIUS_CAKEPHP_Rest:: Invalid JSON returned with message ['.
					AWIT_WHMCS_Util::json_last_error_msg().'] from URL ['.$url.']');

		}

		return $data;
	}



	/**
	 * @method httpErrors
	 * Displays errors messages specific to HTTP Codes
	 *
	 */
	public function httpErrors($httpCode = 200) {
		// Handling Unauthorized
		if (intval($httpCode) === 401) {
			throw new Exception("Authentication Error:: You are not authorized to access this resource.");
		} else if (intval($httpCode) === 500) {
			throw new Exception("Internal Server Error:: The server encountered an internal error");
		} else if (intval($httpCode) === 404) {
			throw new Exception("Server Error:: Page not found");
		}

		return false;
	}



	/**
	 * @method errors
	 * This method prints all error info to the screen
	 *
	 * @param $data the results that contain error information
	 * @return (bool) True/False if any errors occured
	 */
	public function errors($data) {
		// Account for context prefix
		if (isset($data[$this->prefixData]['status'])) {
			$data = $data[$this->prefixData];
		}

		// Throw exception on fail
		if ($data['status'] === 'fail') {
			// Handle CakePHP Validation Errors
			if (is_array($data['message'])) {
				foreach ($data['message'] as $field => $item) {
					foreach ($item as $message) {
						AWIT_WHMCS_Util::logError("Validation Error: $field", htmlentities($message));
					}
				}
				return true;
			}

			AWIT_WHMCS_Util::logError("Error", htmlentities($data['message']));
			return true;
		}

		// Indicate that there were no errors
		return false;
	}



	/**
	 * @method getPaginationPageTotal
	 * @return Returns the total amount of pages for use with pagination or false if any issues occured
	 */
	public function getPaginationPageTotal() {
		$tmpReadQueryString = $this->readQueryString;
		$tmpPrefixData = $this->prefixData;
		$this->setPrefixData('');

		// Build query string to include pages action
		preg_match_all("/\/(\w*)/", $tmpReadQueryString, $matches, PREG_OFFSET_CAPTURE);

		if (isset($matches[1][1][0])) {
			$action = $matches[1][1][0];
		} else {
			return false;
		}

		$queryString = str_replace($action, "pages", $tmpReadQueryString);

		$this->setReadQueryString($queryString);
		$data = $this->readRawData();

		$this->readQueryString = $tmpReadQueryString;
		$this->setPrefixData($tmpPrefixData);
		if ($data['status'] == 'success') {
			if (isset($data['data']['pages']['count'])) {
				return intval($data['data']['pages']['count']);
			}
		}

		return false;
	}



	/**
	 * @method getPaginationPageCount
	 * @return Returns the amount of pages for use with pagination
	 */
	public function getPaginationPageLimit() {
		$tmpReadQueryString = $this->readQueryString;
		$tmpPrefixData = $this->prefixData;
		$this->setPrefixData('');

		// Build query string to include pages action
		preg_match_all("/\/(\w*)/", $tmpReadQueryString, $matches, PREG_OFFSET_CAPTURE);

		if (isset($matches[1][1][0])) {
			$action = $matches[1][1][0];
		} else {
			return false;
		}

		$queryString = str_replace($action, "pages", $tmpReadQueryString);

		$this->setReadQueryString($queryString);
		$data = $this->readRawData();

		$this->readQueryString = $tmpReadQueryString;
		$this->setPrefixData($tmpPrefixData);
		if ($data['status'] == 'success') {
			if (isset($data['data']['pages']['limit'])) {
				return intval($data['data']['pages']['limit']);
			}
		}

		return false;
	}



}

# vim: ts=4
