<?php
/**
 * AWIT SMRADIUS - SMRadius Rest Client
 * Copyright (c) 2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Make sure we being used from within whmcs framework for functions like logModuleCall etc.
if (!defined("WHMCS")) {
	die("This file cannot be accessed directly");
}



/**
 * @class AWIT_WHMCS_Data_Drivers_Mysql
 * @brief The Mysql Driver for use with the AWIT_WHMCS_Data_Connection class
 *
 * This class exposes methods needed to interact with Mysql databases
 * and provides data formatted to work with the display classes
 */
class AWIT_WHMCS_Data_Drivers_Mysql extends AWIT_WHMCS_Data_Connection
{



	/**
	 * @var $config
	 * The configuration for performing operations on the database
	 *
	 * ```
	 * Setting up configuration to connect to alternate host
	 * $config['host'] = 'localhost';
	 * $config['port'] = '3306'; // Defaults to 3306
	 * $config['charset'] = 'utf8'; // Defaults to utf8
	 * $config['user'] = 'root';
	 * $config['password'] = 'password';
	 * $config['database'] = 'thedatabase';
	 * NOTES: will only use the alternate resource apon successful connection
	 *        as whmcs already has a connection
	 * ```
	 *
	 */
	private $config;



	/**
	 * @var $schema
	 * The schema against which to perform database operations
	 *
	 * ```
	 * Import constraints to check for duplicates when inserting
	 * $schema['uniquekey'] = array('field1', 'field2', 'field3')
	 *
	 * Specifying table
	 * $schema['awit_tbl_tablename'] = array (
	 *		'fields' => array('field1', 'field2')
	 * );
	 *
	 * Specifying fields (Must conform to valid field names for Mysql)
	 * $schema['fields'] = array('field1', 'field2');
	 *
	 * ```
	 *
	 */
	private $schema;



	/**
	 * @var $table
	 * The table to perform operations on
	 */
	private $table;



	/**
	 * @var $dbh
	 * The database connection handle
	 */
	private $dbh;



	/**
	 * @var $sth
	 * The prepared statement handle
	 */
	private $sth;



	/**
	 * @method __construct
	 * Constructs the AWIT_WHMCS_Data_Drivers_Mysql object
	 */
	public function __construct($config = array(), $schema = array())
	{
		$this->config = $config;

		$this->dbh = $this->getConnection();

		if (!empty($schema)) {
			$this->schema = new AWIT_WHMCS_Data_Schema($schema);
		}
	}



	/**
	 * @method getConnection
	 * Gets a PDO connection to the configured database
	 *
	 * @return Returns the PDO db handle
	 */
	public function getConnection() {
		if (!is_object($this->dbh)) {
			try {
				if (!empty($this->config)) {

					// Check host
					if (isset($this->config['host'])) {
						$db_host = $this->config['host'];
					} else {
						throw new Exception("Mysql Config Error:: Host not set");
					}

					// Check port
					if (isset($this->config['port'])) {
						$db_port = $this->config['port'];
					} else {
						$db_port = '3306';
					}

					// Check charset
					if (isset($this->config['charset'])) {
						$db_charset = $this->config['charset'];
					} else {
						$db_charset = 'utf8';
					}

					// Check user
					if (isset($this->config['user'])) {
						$db_username = $this->config['user'];
					} else {
						throw new Exception("Mysql Config Error:: User not set");
					}

					// Check password
					if (isset($this->config['password'])) {
						$db_password = $this->config['password'];
					} else {
						throw new Exception("Mysql Config Error:: Password not set");
					}

					// Check database
					if (isset($this->config['database'])) {
						$db_name = $this->config['database'];
					} else {
						throw new Exception("Mysql Config Error:: Database not set");
					}

				} else {
					// Default to using WHMCS database config
					require dirname( __FILE__ ) . '/../../../../../../../../configuration.php';
					$db_charset = 'utf8';
				}

				$this->dbh = new PDO("mysql:host=$db_host;port=3306;dbname=$db_name;charset=$db_charset",
						$db_username, $db_password);
				$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (PDOException $ex) {
				throw new Exception("PDO Error:: Couldn't connect to the database: " . $ex->getMessage());
			}
		}

		return $this->dbh;
	}



	/**
	 * @method getProvider
	 * Gets the provider
	 */
	public function getProvider()
	{
		if (!is_object($this->provider)) {
			$class = get_class($this);
			$this->provider = new $class($this->config);
		}

		return $this->provider;
	}



	/**
	 * @method close
	 * Closes the connection to the database
	 */
	public function close()
	{
		if (is_object($this->dbh)) {
			$this->dbh = null;
		}
	}



	/**
	 * @method getConfig
	 * Gets the config
	 */
	public function getConfig()
	{
		return new AWIT_WHMCS_Data_Config($this->config);
	}



	/**
	 * @method setConfig
	 * Sets the config
	 */
	public function setConfig($config)
	{
		$this->config = $config;
	}



	/**
	 * @method getSchema
	 * Gets the schema
	 */
	public function getSchema()
	{
		if (is_object($this->schema) && get_class($this->schema) == 'AWIT_WHMCS_Data_Schema') {
			return $this->schema;
		} else if (is_array($this->schema)) {
			return new AWIT_WHMCS_Data_Schema($this->schema);
		}

		return array();
	}



	/**
	 * @method setSchema
	 * Sets the schema
	 */
	public function setSchema($schema)
	{
		$this->schema = new AWIT_WHMCS_Data_Schema($schema);
		$this->provider = $this->getProvider();
	}



	/**
	 * @method getTable
	 * Gets the table
	 */
	public function getTable()
	{
		$schema = $this->getSchema()->getSchema();
		if (isset($schema['table'])) {
			$this->table = $schema['table'];
		} else {
			throw new Exception('AWIT_WHMCS_Data_Drivers_Mysql:: Table not set');
		}

		return $this->table;
	}



	/**
	 * @method setTable
	 * Sets the table
	 */
	public function setTable($table)
	{
		$this->getSchema()->setSchemaPart(array('table' => $table));
		$this->table = $table;
	}



	/**
	 * @method setResource
	 * Sets the resourceId for the driver
	 */
	public function setResource($resource)
	{
		$this->setTable($resource);
	}



	/**
	 * @method getResource
	 * Gets the resource for the driver
	 *
	 * @return $table
	 */
	public function getResource()
	{
		return $this->getTable();
	}



	/**
	 * @method query
	 * Performs PDO queries from prepared statements
	 *
	 * @return $res Rows affected
	 */
	public function query($query, $params = array()) {
		try {
			$this->sth = $this->dbh->prepare($query);
			$res = $this->sth->execute($params);
			return $res;
		} catch (PDOException $ex) {
			throw new Exception("Could'nt execute query:: " . $ex->getMessage());
		}
	}

	/**
	 * @method readData
	 * Reads data from the specified data source
	 * Will read all data unless matches are specified
	 *
	 * @param $matches An array of field/value pairs to match by
	 * @return $data An array of key/value paired records
	 */
	public function readData($matches = array())
	{
		$this->table = $this->getTable();

		$schema = $this->schema->find($this->table);

		// Fetching client refs from database
		$query = sprintf("
				SELECT
					%s
				FROM
					%s
			",
			implode(",", $schema['fields']),
			$this->table
		);

		if (!empty($matches)) {
			$query .= "WHERE " .
					implode(' = ? AND ', array_filter(array_keys($matches), array($this->dbh, 'quote'))) . ' = ?';

			$sthParams = array_values($matches);
		}

		// Execute query
		$this->query($query, $sthParams);

		$data = array();
		while ($row = $this->sth->fetch(PDO::FETCH_ASSOC)) {
			array_push($data, $row);
		}

		return $data;
	}



	/**
	 * @method writeData
	 * Writes data to the specified data source
	 * Checks for duplicates if specified
	 *
	 * @param $data array of records in the form
	 *        array (
	 *            array('field' => 'value'),
	 *            array('field' => 'value')
	 *        )
	 *        OR just a single record
	 *        array('field' => 'value')
	 *
	 * @param $matches Where updates field/value pairs can be specified
	 *        Only records matching these will be updated
	 * @return Returns array('imported', 'duplicates')
	 * @throws Throws exceptions on any mysql errors
	 */
	public function writeData($data, $matches = array()) {
		if (!is_array($data)) {
			return false;
		}

		if (empty($data)) {
			return false;
		}

		$this->table = $this->getTable();

		$schema = $this->schema->find($this->table);

		$queued = $data;
		$isMultiple = false;

		// Checking for multiple records
		if (is_array($data[0])) {
			$isMultiple = true;
		}

		// If multiple records specified do duplicate checking for all
		if ($isMultiple) {

			if (!empty($schema['uniquekey'])) {

				// Checking for duplicates
				$query = sprintf("
						SELECT
							count(*) AS cnt,
							%s
						FROM
							%s
						WHERE
					",
					implode(",", $schema['uniquekey']),
					$this->table
				);

				// Building where clause to match on any unique constraint
				$uniqueKeys = array_intersect_key($data, $schema['uniquekey']);
				$query .= implode(' = ? OR ', array_keys($uniqueKeys)) . ' = ?';
				$sthParams = array_values($uniqueKeys);

				$query .= sprintf("
						GROUP BY %s
						HAVING cnt > 0
					",
					implode(",", $schema['uniquekey'])
				);

				// Execute query
				$this->query($query, $sthParams);

				// Have all entries which represent duplicates, will remove them from the queue
				while ($row = $this->sth->fetch(PDO::FETCH_ASSOC)) {

					// Deleting duplicate records from queue
					$isDuplicate = false;
					foreach ($data as $key => $item) {
						foreach ($schema['uniquekey'] as $field) {
							// Match against all duplicate designated fields
							if ($row[$field] == $item[$field]) {
								$isDuplicate = true;
							}
						}

						// Only queue non-duplicates
						if ($isDuplicate) {
							unset($queued[$key]);
						}

					}

				}

			}
		} else {

			// Performing operation for single record
			$item = $data;

			// Only performing check for single item
			if (!empty($schema['uniquekey'])) {

				// Checking for duplicates
				$query = sprintf("
						SELECT
							count(*) AS cnt
						FROM
							%s
						WHERE
					",
					$this->table
				);

				// Building where clause to match on any unique constraint
				$uniqueKeys = array_intersect_key($data, $schema['uniquekey']);
				$query .= implode(' = ? AND ', array_keys($uniqueKeys)) . ' = ?';
				$sthParams = array_values($uniqueKeys);

				// Execute query
				$this->query($query, $sthParams);

				// Fetching count of duplicates, if any
				$row = $this->sth->fetch(PDO::FETCH_ASSOC);
				$count = $row['cnt'];

				// If no duplicates, insert row
				if ($count == 0) {
					array_push($queued, $item);
				}
			}

		}

		// Return false if none to import
		if (empty($queued)) {
			return 0;
		}

		// If matches specified, perform UPDATE
		if (!empty($matches)) {

			// Multiple update
			if ($isMultiple) {
				// Multiple records specified for a single update
				// using the first records data
				$item = $data[0];
			}

			// Building UPDATE Query
			$query = sprintf("
						UPDATE %s
						SET
					",
				$this->table
			);

			// Building SET clause
			$query .= implode(' = ?,', array_keys($item));
			$sthParams = array_values($item);

			$query .= "
				WHERE
			";

			// Building where clause to match any specified field values
			$query .= implode(' = ?,', array_keys($matches));
			$sthParams = array_values($matches);

			// Execute query
			$count = $this->query($query, $sthParams);

			return $count;

		} else {
			// Perform INSERT

			// Building INSERT Query
			$query = sprintf("INSERT INTO %s (", $this->table);

			// Building where clause to match any specified field values
			$query .= implode(',', $schema['fields']);

			// Building Values
			$query .= ")
				VALUES
			";

			// Multiple insert
			if ($isMultiple) {

				// Build multiple insert statement
				$queuedInsertValues = array();
				foreach ($queued as $item) {
					array_push($queuedInsertValues, implode('?,', $item));
					$sthParams = array_values($item);
				}

				$query .= '(' . implode('),(', $queuedInsertValues) . ')';

			} else {

				// Inserting single entry
				$query .= '(' . implode('?,', $item) . ')';
				$sthParams = array_values($item);

			}

			// Execute query
			$this->query($query, $sthParams);

			return count($queued);
		}
	}



}

