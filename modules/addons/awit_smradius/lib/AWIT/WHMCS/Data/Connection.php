<?php
/**
 * AWIT SMRADIUS - Bank Transaction Processing Class
 * Copyright (c) 2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we being used from within whmcs framework for functions like logModuleCall etc.
if (!defined("WHMCS")) {
	die("This file cannot be accessed directly");
}


/**
 * @class AWIT_WHMCS_Data_Connection
 * @brief Provides an abstract mechanism for reading and writing data
 *        using different drivers ie REST vs Mysql
 *
 * This class will set up as lightly as possible whats needed to connect
 * to and perform operations on a specific data source using the specified
 * provider.
 */
class AWIT_WHMCS_Data_Connection
{



	/**
	 * @var $dataset
	 * Contains the key/value paired dataset
	 */
	private $dataset;



	/**
	 * @var $config
	 * The configuration to send to the driver
	 */
	private $config;



	/**
	 * @var $schema
	 * The schema against which to perform database operations
	 */
	private $schema;



	/**
	 * @var $driver
	 * The driver/class to use when performing data operations
	 */
	private $driver;



	/**
	 * @var $provider
	 * The object to use when performing data operations
	 */
	private $provider;



	/**
	 * @var $resource
	 * Either the table or the query string depending on which driver
	 */
	private $resource;



	/**
	 * @method __construct
	 * @brief Constructs the AWIT_WHMCS_Data_Connection object
	 *
	 * @param $provider
	 */
	public function __construct($provider = null) {
		if (is_object($provider)) {
			$this->provider = $provider;
		} else if (!empty($this->driver)){
			new $this->driver($this->config, $this->schema);
		}
	}



	/**
	 * @method getConfigObject
	 * gets the config object
	 *
	 * @return The config object
	 */
	public function getConfigObject()
	{
		$this->provider = $this->getProvider();
		return $this->provider->getConfig();
	}



	/**
	 * @method getConfig
	 * Gets the config
	 */
	public function getConfig()
	{
		$this->provider = $this->getProvider();
		return $this->provider->getConfig()->getConfig();
	}



	/**
	 * @method setConfig
	 * Sets the config
	 *
	 * @param $config Array of configuration options
	 */
	public function setConfig($config)
	{
		$this->provider = $this->getProvider();
		return $this->provider->setConfig($config);
	}



	/**
	 * @method getSchemaObject
	 * Gets the schema object
	 *
	 * @return The schema object
	 */
	public function getSchemaObject()
	{
		$this->provider = $this->getProvider();
		return $this->provider->getSchema();
	}



	/**
	 * @method getSchema
	 * Gets the schema
	 *
	 * @return The schema array
	 */
	public function getSchema()
	{
		$this->provider = $this->getProvider();
		return $this->provider->getSchema()->getSchema();
	}



	/**
	 * @method setSchema
	 * Sets the schema
	 *
	 * @param $schema The schema array
	 */
	public function setSchema($schema)
	{
		$this->provider = $this->getProvider();
		$this->provider->setSchema($schema);
	}



	/**
	 * @method setSchemaPart
	 * Sets a portion of the schema array to the values specified in the params array
	 * identified by the array keys
	 *
	 * @param $params The schema array
	 */
	public function setSchemaPart($params)
	{
		if (!empty($params)) {
			$this->provider->getSchema()->setSchemaPart($params);
		} else {
			return false;
		}
	}



	/**
	 * @method setTable
	 * Sets the table in the schema for the driver that supports it
	 *
	 * @param $table The table to perform operations on
	 */
	public function setTable($table)
	{
		$this->provider->getSchema()->setSchemaPart(array('table' => $table));
	}



	/**
	 * @method setQueryString
	 * Sets the query string
	 *
	 * @param $readQueryString The query string to use for read operations
	 */
	public function setReadQueryString($readQueryString)
	{
		$this->provider->setReadQueryString($readQueryString);
	}



	/**
	 * @method setQueryString
	 * Sets the query string
	 *
	 * @param $writeQueryString The query string to use for write operations
	 */
	public function setWriteQueryString($writeQueryString)
	{
		$this->provider->setWriteQueryString($writeQueryString);
	}



	/**
	 * @method setPrefixData
	 * Sets the prefixDataId for the driver
	 *
	 * @param $prefixData The prefix string to use on data
	 * @return (bool) True / False
	 */
	public function setPrefixData($prefixData)
	{
		if (is_object($this->provider) && $this->provider instanceof AWIT_WHMCS_Data_Drivers_CakePhpRest) {
			$this->provider->setPrefixData($prefixData);
			return true;
		}

		return false;
	}



	/**
	 * @method getPrefixData
	 * Gets the prefixData for the driver
	 *
	 * @return $this->provider->prefixData
	 */
	public function getPrefixData()
	{
		if (is_object($this->provider) && $this->provider instanceof AWIT_WHMCS_Data_Drivers_CakePhpRest) {
			return $this->provider->getPrefixData();
		}

		return false;
	}



	/**
	 * @method getDriver
	 * Gets the driver
	 */
	public function getDriver()
	{
		return $this->driver;
	}



	/**
	 * @method setDriver
	 * Sets the driver
	 *
	 * @param $driver The driver's class name as a string
	 */
	public function setDriver($driver)
	{
		$this->driver = $driver;
	}



	/**
	 * @method getProvider
	 * Gets the provider
	 *
	 * @return $this->provider
	 */
	public function getProvider() {
		if (empty($this->driver)) {
			throw new Exception('AWIT_WHMCS_Data_Connection:: Driver not set');
		}
		if (!is_object($this->provider)) {
			$this->provider = new $this->driver($this->config, $this->schema);
		}

		return $this->provider;
	}



	/**
	 * @method readData
	 * Reads data from the specified data source
	 *
	 * @param $matches Array of key/value pairs used to filter records
	 */
	public function readData($matches = array()) {
		return $this->provider->readData($matches);
	}



	/**
	 * @method getResourceId
	 * Gets the resourceId for the driver
	 *
	 * @return The drivers resourceId
	 */
	public function getResourceId()
	{
		$this->provider = $this->getProvider();
		return get_class($this->provider);
	}



	/**
	 * @method setResource
	 * Sets the resourceId for the driver
	 *
	 * @param $resource
	 * @return (bool) True / False
	 */
	public function setResource($resource)
	{
		$this->provider = $this->getProvider();
		$this->resource = $this->provider->setResource($resource);
	}



	/**
	 * @method getResource
	 * Gets the resource for the driver
	 *
	 * @return (bool) True / False
	 */
	public function getResource()
	{
		$this->provider = $this->getProvider();
		return $this->provider->getResource();
	}



	/**
	 * @method writeData
	 * Writes data to the specified data source
	 *
	 * @param $data Data to write
	 * @param $matches Array of key/value pairs used to filter records
	 * @return true/false
	 */
	public function writeData($data, $matches = array()) {
		return $this->provider->writeData($data, $matches);
	}



	/**
	 * @method handleErrors
	 * Handles errors like validation per driver
	 *
	 * @param $data Contains an array of collected errors
	 */
	public function errors($data = array())
	{
		$this->provider = $this->getProvider();
		return $this->provider->errors($data);
	}



}

# vim: ts=4
