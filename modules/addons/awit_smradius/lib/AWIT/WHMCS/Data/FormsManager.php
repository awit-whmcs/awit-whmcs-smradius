<?php
/**
 * AWIT SMRADIUS - Bank Transaction Processing Class
 * Copyright (c) 2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we being used from within whmcs framework for functions like logModuleCall etc.
if (!defined("WHMCS")) {
	die("This file cannot be accessed directly");
}



/**
 * @class AWIT_WHMCS_Html_FormsManager
 * @brief The class responsible for keeping a registry of all forms
 *
 * This class exposes methods needed to traverse, query and amend any given form
 * definitions
 */
class AWIT_WHMCS_Html_FormsManager extends AWIT_WHMCS_Data_Array
{



	/**
	 * @var $entityManager
	 *
	 * Sets the entityManager for any operations on the data source
	 *
	 */
	private $entityManager;



	/**
	 * @method setEntityManager
	 *
	 * Sets the entityManager for use with operations on the data source
	 *
	 * @return $entityManager
	 */
	public function setEntityManager($entityManager)
	{
		if (is_object($entityManager)) {
			$this->entityManager = $entityManager;
		}

		return false;
	}



	/**
	 * @method getForms
	 * Gets the form definitions
	 */
	public function getForms()
	{
		return $this->inputArray;
	}



	/**
	 * @method setForms
	 * Sets the form definitions
	 */
	public function setForms($schema)
	{
		$this->inputArray = $schema;
	}



	/**
	 * @method setSchemaPart
	 * Sets a portion of the schema array to the values specified in the params array
	 * identified by the array keys
	 *
	 */
	public function setFormsPart($params)
	{
		$this->set($params);
	}



	/**
	 * @method getForms
	 *
	 * @param $formKey Key at which the form definition is stored
	 * @return $form Array of elements for the form
	 */
	public function getForm($formKey)
	{
		return $this->find($formKey);
	}



	/**
	 * @method processForm
	 * Processes form submissions
	 *
	 * @param $formKey
	 */
	public function processForm($formKey, $resource = '') {
		// If CakePHP_Rest driver get the query string to write to
		if (empty($resource)) {
			if ($this->entityManager->getProvider() instanceof AWIT_WHMCS_Data_Drivers_CakePhpRest) {
				$resource = $_REQUEST[$this->entityManager->getResourceId()];
				$this->entityManager->setWriteQueryString($resource);
			}
		}

		if (!empty($formKey)) {
			try {
				$elements = $this->getForm($formKey);
				$form = new AWIT_WHMCS_Html_Form($id);
				$form->setEntityManager($this->entityManager);
				$form->setElements($elements);
				$form->processForm();

			} catch (Exception $ex) {
				$message = $ex->getMessage();
				if (strpos($message, "::") > -1) {
					$messageParts = explode("::", $message);
					AWIT_WHMCS_Util::logError($messageParts[0], $messageParts[1]);
				} else {
					AWIT_WHMCS_Util::logError("Form Processing Error", htmlentities($ex->getMessage()));
				}
			}

		}

		return false;
	}
}

