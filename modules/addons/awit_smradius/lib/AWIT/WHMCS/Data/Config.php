<?php
/**
 * AWIT SMRADIUS - SMRadius Rest Client
 * Copyright (c) 2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we being used from within whmcs framework for functions like logModuleCall etc.
if (!defined("WHMCS")) {
	die("This file cannot be accessed directly");
}



/**
 * @class AWIT_WHMCS_Data_Config
 * @brief The config class for traversing any config in a node tree fashion
 *
 * This class exposes methods needed to traverse, query and amend any given configuration
 * definitions needed to perform any specific task by any of the higher layers
 */
class AWIT_WHMCS_Data_Config extends AWIT_WHMCS_Data_Array
{



	/**
	 * @method getConfig
	 * Gets the config
	 */
	public function getConfig()
	{
		return $this->inputArray;
	}



	/**
	 * @method setConfig
	 * Sets the config
	 *
	 * @param $config
	 * @return $this->inputArray
	 */
	public function setConfig($config)
	{
		$this->inputArray = $config;
	}



	/**
	 * @method setConfigPart
	 * Sets a portion of the config array to the values specified in the params array
	 * identified by the array keys
	 *
	 * @param $params
	 */
	public function setConfigPart($params)
	{
		$this->set($params);
	}



}

# vim: ts=4
