<?php
/**
 * AWIT SMRADIUS - Bank Transaction Processing Class
 * Copyright (c) 2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we being used from within whmcs framework for functions like logModuleCall etc.
if (!defined("WHMCS")) {
	die("This file cannot be accessed directly");
}



/**
 * @class AWIT_WHMCS_Data_Schema
 * @brief The schema class for traversing any schema in a node tree fashion
 *
 * This class exposes methods needed to traverse, query and amend any given schema
 * definitions needed to perform any specific task by any of the higher layers
 */
class AWIT_WHMCS_Data_Schema extends AWIT_WHMCS_Data_Array
{

	/**
	 * @method getSchema
	 * gets the schema
	 */
	public function getSchema()
	{
		return $this->inputArray;
	}



	/**
	 * @method setSchema
	 * sets the schema
	 */
	public function setSchema($schema)
	{
		$this->inputArray = $schema;
	}



	/**
	 * @method setSchemaPart
	 * sets a portion of the schema array to the values specified in the params array
	 * identified by the array keys
	 *
	 */
	public function setSchemaPart($params)
	{
		$this->set($params);
	}

}

# vim: ts=4
