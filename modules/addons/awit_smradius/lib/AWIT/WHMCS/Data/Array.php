<?php
/**
 * AWIT SMRADIUS - AWIT_WHMCS_Data_Array Class
 * This class exposes methods needed to query and amend and perform operations on arrays
 *
 * Copyright (c) 2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we being used from within whmcs framework for functions like logModuleCall etc.
if (!defined("WHMCS")) {
	die("This file cannot be accessed directly");
}



/**
 * @class AWIT_WHMCS_Data_Array
 * @brief This class contains methods which help with searching or amending specific.
 *        portions of any array. Exposes findPaths which prints the path structure for an array.
 *        Exposes method "unsetByPath" which unsets any given portion of an array denoted by path.
 *
 * This class exposes methods needed to query and amend any given array
 */
class AWIT_WHMCS_Data_Array
{


	/**
	 * @var $inputArray
	 * The inputArray to traverse, amend and query
	 *
	 * The inputArray is an array of anything meant to define or collectively arrange
	 * options or configuration items needed for tasks specific to the utility it's
	 * meant to service
	 *
	 */
	protected $inputArray;


	/**
	 * @var $path
	 * The path to the array node to modify
	 *
	 * When setting items in the array the recursive function keeps
	 * a record of the path to the item which is used to set the specified
	 * value
	 */
	private $path;


	/**
	 * @var $resultArray
	 * The private variable used by findAll and unsetByPath
	 */
	private $resultArray;


	/**
	 * @var $pointerResultArray
	 * The private variable used by unsetByPath to store
	 * an array of pointers to array parts which constitutes the inputArray
	 */
	private $pointerResultArray;


	/**
	 * @var $unsetByPathIndex
	 * The private variable used by unsetByPath to keep
	 * track of previously declared pointers
	 */
	private $unsetByPathIndex;


	/**
	 * @var $resultPath
	 * The private variable used by findPaths, getPath and resetPath
	 */
	private $recurseDepth;


	/**
	 * @var $resultPath
	 * The private variable used by findPaths, getPath and resetPath
	 */
	private $resultPath;



	/**
	 * @method __construct
	 * Constructs the AWIT_WHMCS_Data_Drivers_Mysql object
	 */
	public function __construct($inputArray = array())
	{
		if (!empty($inputArray) && is_array($inputArray)) {
			$this->inputArray = $inputArray;
		}

		$this->resultArray = array();
		$this->pointerResultArray = array();
		$this->unsetByPathIndex = -1;
		$this->recurseDepth = 0;
	}



	/**
	 * @method find
	 * This method recursively loops over all the items in the inputArray array
	 * and returns the value where the first occuring matching key was found
	 *
	 * e.g. array(
	 *		'meta' => 'This is the inputArray which represents a standard database table'
	 *		'thetable_1' => array(
	 *			'fields' => array(
	 *				'name', 'surname', 'sgroup'
	 *			)
	 *		),
	 *		'thetable_2' => array(
	 *			'fields' => array(
	 *				'name', 'surname', 'sgroup'
	 *			)
	 *		)
	 *	)
	 *
	 * $this->find('meta'); // Returns 'This is the inputArray which represents a standard database table'
	 * $this->find('thetable_1'); // Returns the array at that key
	 *
	 * @param $keyIn the key to search for
	 * @return $inputArrayPart The child node identified by this key
	 */
	public function find($keyIn, $inputArrayPart = array()) {
		if (empty($this->inputArray)) {
			return false;
		}

		$inputArray = $this->inputArray;
		if (!empty($inputArrayPart)) {
			$inputArray = $inputArrayPart;
		}

		foreach ($inputArray as $key => $value) {
			if ($key === $keyIn) {
				return $value;
			}
			if (is_array($value)) {
				$this->find($keyIn, $value);
			}
		}
	}



	/**
	 * @method findAll
	 * This method recursively loops over all the items in the inputArray array
	 * and returns an array of child values or arrays where the matching key
	 * was found
	 *
	 * This method internally populates the path array with arrays containing
	 * the paths to each at their respective indexes.
	 *
	 * e.g. array(
	 *           'meta' => 'This is the inputArray which represents a standard database table'
	 *           'thetable_1' => array(
	 *               'fields' => array(
	 *                   'name', 'surname', 'sgroup'
	 *           )
	 *      ),
	 *           'thetable_2' => array(
	 *               'fields' => array(
	 *                   'name', 'surname', 'sgroup'
	 *               )
	 *           )
	 *      )
	 *
	 * $this->findAll('fields');
	 * // Returns an array containing the value at those keys
	 * e.g. array(
	 *           0 => array(
	 *                'name',
	 *                'surname',
	 *                'sgroup'
	 *           ),
	 *           1 => array(
	 *                'name',
	 *                'surname',
	 *                'sgroup'
	 *           )
	 *     )
	 *
	 * @param $params An array of keys to search for
	 * @return $this->resultArray The child nodes identified by the keys in the params array
	 */
	public function findAll($params, $inputArrayPart = array()) {
		// Reset resultPath, clear from previous calls to findAll
		if (empty($this->resultPath)) {
			$this->resultPath = array();
		}

		// Check for valid input array
		if (empty($this->resultArray)) {
			$this->resultArray = array();
		}

		// Setting index path to depth for recursion
		if (empty($this->path)) {
			$this->path = array();
		}

		// Check for valid input array
		if (empty($this->inputArray)) {
			return false;
		}

		// Check for tmpInputArray
		if (empty($this->tmpInputArray)) {
			$this->tmpInputArray = $this->inputArray;
		}

		// Set tmp array
		$inputArray = $this->inputArray;
		if (!empty($inputArrayPart)) {
			$inputArray = $inputArrayPart;
		}

		// Searching params to extract key, value and the path
		if (!is_array($params)) {
			$params = array($params);
		}



		foreach ($params as $paramValue) {
			foreach ($inputArray as $key => $value) {
				if ($key === $paramValue) {
					$resultKey = $paramKey;
					$resultValue = $value;

					// append item to results array
					array_push($this->resultArray, $resultValue);

					// Add path to collected array
					array_push($this->path, $key);
					array_push($this->resultPath, implode('/', $this->path));

					$this->tmpInputArray = $this->unsetByPath($this->tmpInputArray, $this->path);

					$matches = $this->find($paramValue, $this->tmpInputArray);
					if (!empty($matches)) {
						$this->matchesFound = true;
					}

					// Reset path
					$this->path = array();
				}

				// Searching for matches in child nodes
				if (!empty($value) && is_array($value)) {
					array_push($this->path, $key);
					$tmpArr = $params;
					unset($params[$paramKey]);
					$this->findAll($params, $value);
					$params = $tmpArr;
				}

			}

			if ($this->matchesFound) {
				// Find the other matches
				$this->matchesFound = false;
				$this->findAll($params, $this->tmpInputArray);
			}
		}

		// Reset the path
		$this->path = array();

		return $this->resultArray;
	}



	/**
	 * @method findPaths
	 * This method recursively loops over all the items in the inputArray array
	 * and returns an array of paths to each element
	 *
	 * @param $inputArrayPart the array to extract paths for
	 * @return $paths An array of paths for the given array
	 */
	public function findPaths($inputArrayPart = array()) {
		// Reset resultPath, clear from previous calls to findAll
		if (empty($this->resultPath)) {
			$this->resultPath = array();
		}

		// Setting index path to depth for recursion
		if (empty($this->path)) {
			$this->path = array();
		}

		// Set tmp array
		$inputArray = $this->inputArray;
		if (!empty($inputArrayPart)) {
			$inputArray = $inputArrayPart;
		}

		if (!is_array($inputArray)) {
			return false;
		}

		// Looping through inputArray to extract paths
		foreach ($inputArray as $key => $value) {
			if (!is_array($value)) {
				// Add path to collected array
				// Check for associative values over integer
				if (is_string($key)) {
					array_push($this->path, $key);
				} else {
					array_push($this->path, $value);
				}

				array_push($this->resultPath, implode('/', $this->path));

				// Temp Array
				$this->tmpInputArray = $this->unsetByPath($this->tmpInputArray, $this->path);

				// Pop off the last element
				array_pop($this->path);
			} else if (empty($value) && is_array($value)) {
				// Add path to collected array
				// Check for associative values over integer
				if (is_string($key)) {
					array_push($this->path, $key);
				} else {
					array_push($this->path, $value);
				}

				array_push($this->resultPath, implode('/', $this->path));

				// Temp Array
				$this->tmpInputArray = $this->unsetByPath($this->tmpInputArray, $this->path);

				// Pop off the last element
				array_pop($this->path);
			}

			// Searching for matches in child nodes
			if (!empty($value) && is_array($value)) {
				array_push($this->path, $key);
				array_push($this->resultPath, implode('/', $this->path));
				unset($params[$paramKey]);
				$this->findPaths($value);
			}
		}

		// Pop off the last element
		array_pop($this->path);

		return $this->resultPath;
	}



	/**
	 * @method getPath
	 * Returns the value of the path variable
	 */
	public function getPath() {
		return $this->resultPath;
	}



	/**
	 * @method resetPath
	 * Resets the path array to empty
	 */
	public function resetPath() {
		$this->resultPath = array();
	}



	/**
	 * @method unsetByPath
	 * Deletes the portion of an array denoted by the path
	 *
	 * @param $inputArray the source array
	 * @param $targetPath The array or slash separated string denoting the path
	 * @return $resultArray The new array after deleting the item
	 */
	function unsetByPath($inputArray, $targetPath = array()) {
		// If called before reset
		if ($this->recurseDepth == -1) {
			$this->unsetByPathIndex = -1;
			$this->pointerResultArray = array();
			$this->resultArray = array();
			$this->recurseDepth = 0;
			$this->path = array();
		}

		if (is_array($targetPath)) {
			$unsetPath = join('/', $targetPath);
		} else {
			$unsetPath = $targetPath;
		}

		$isAssociativeArray = AWIT_WHMCS_Util::is_assoc_array($inputArray);
		foreach ($inputArray as $key => $value) {
			if (!$isAssociativeArray && !is_array($value)) {
				$label = $value;

			} else if (is_array($value)) {
				$this->unsetByPathIndex++;
				$label = $key;

				// Adding current key to path
				array_push($this->path, $key);

				// Skipping if this matches the specified target path
				$comparePath = join('/', $this->path);
				if ($unsetPath == $comparePath) {
					// Popping end of the path
					array_pop($this->path);
					$currentPath = end($this->path);
					if (count($this->path) > 0) {
						$this->pointerResultArray[$this->unsetByPathIndex] =&
								$this->pointerResultArray[$this->unsetByPathIndex-count($this->path)];
					}

					continue;
				}

				$currentPath = end($this->path);

				if (empty($this->resultArray) || count($this->path) == 1) {
					// step 1: key, tbl_awit_smradius_users
					$this->resultArray[$currentPath] = array();
					$this->pointerResultArray[$this->unsetByPathIndex] =& $this->resultArray[$currentPath];

				} else {
					// step 2: key, fields
					$this->pointerResultArray[$this->unsetByPathIndex-1][$currentPath] = array();
					$this->pointerResultArray[$this->unsetByPathIndex] =&
							$this->pointerResultArray[$this->unsetByPathIndex-1][$currentPath];

				}

			}

			if (is_array($value)) {
				// Recursing the child array
				$this->recurseDepth++;
				$this->unsetByPath($value, $targetPath);
			} else {
				// Adding leaf nodes
				if (!is_null($this->pointerResultArray[$this->unsetByPathIndex-1])) {
					if (!$isAssociativeArray) {
						$this->pointerResultArray[$this->unsetByPathIndex][] = $label;
					} else {
						$this->pointerResultArray[$this->unsetByPathIndex][$key] = $value;
					}
				}
			}
		}

		// Popping end of the path
		array_pop($this->path);
		$currentPath = end($this->path);
		if (count($this->path) > 0) {
			$this->pointerResultArray[$this->unsetByPathIndex] =&
					$this->pointerResultArray[$this->unsetByPathIndex-count($this->path)];
		}

		$this->recurseDepth--;
		return $this->resultArray;
	}



	/**
	 * @method getArray
	 * Gets the inputArray
	 */
	public function getArray()
	{
		return $this->inputArray;
	}



	/**
	 * @method setArray
	 * Sets the inputArray
	 *
	 * @param $inputArray
	 * @return $inputArray
	 */
	public function setArray($inputArray)
	{
		$this->inputArray = $inputArray;
	}



	/**
	 * @method set
	 * Sets portions of the inputArray by key
	 *
	 * @param &$params Reference to the parameters
	 * @return $this->inputArray
	 */
	public function set(&$params, $inputArrayPart = NULL) {
		// Setting index path to depth for recursion
		if (empty($this->path)) {
			$this->path = array();
		}

		// Check for valid input array
		if (empty($this->inputArray)) {
			return false;
		}

		// Set tmp array
		$inputArray = $this->inputArray;
		if (!empty($inputArrayPart)) {
			$inputArray = $inputArrayPart;
		}

		// Iterating $params
		foreach ($params as $paramKey => $paramValue) {
			foreach ($inputArray as $key => $value) {
				if ($key === $paramKey && $key == end($this->path)) {
					$resultKey = $paramKey;
					$resultValue = $paramValue;
					unset($params[$paramKey]);

					// Setting the value at the key found for the objects inputArray path
					$pointer = NULL;
					foreach ($this->path as $pathKey) {
						if ($pointer == NULL) {
							$pointer = &$this->inputArray[$pathKey];
						} else {
							$pointer = &$pointer[$pathKey];
						}

						// set the key if not set
						if (!isset($pointer) && $pathKey != end($this->path)) {
							$pointer[$pathKey] = array();
						} else if ($pathKey == end($this->path)) {
							$pointer = $resultValue;
						}
					}

					// Reset the path
					$this->path = array();
				}

				// Searching for matches in child nodes
				if (is_array($value)) {
					array_push($this->path, $key);
					$this->set($params, $value);
				}
			}
		}

		// Reset the path
		$this->path = array();

		// If there are still params left append them to the inputArray
		if (!empty($params)) {
			foreach ($params as $paramKey => $paramValue) {
				$this->inputArray[$paramKey] = $paramValue;
			}
		}

		return $this->inputArray;
	}



}
