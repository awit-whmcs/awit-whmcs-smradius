<?php
/**
 * AWIT SMRADIUS - SMRadius Rest Client
 * Copyright (c) 2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we being used from within whmcs framework for functions like logModuleCall etc.
if (!defined("WHMCS")) {
	die("This file cannot be accessed directly");
}


/**
 * @class AWIT_WHMCS_Html_Form
 * Assists with rendering of HTML Forms and Elements
 */
class AWIT_WHMCS_Html_Form extends AWIT_WHMCS_Data_Connection
{



	/**
	 * @var $title The title of the datagrid (optional)
	 */
	private $title;



	/**
	 * @var $description The title of the datagrid (optional)
	 */
	private $description;



	/**
	 * @var $elements The title of the datagrid (optional)
	 */
	private $elements;



	/**
	 * @var $populateForm
	 *
	 * Sets the populateForm to indicate weather or not the form
	 * should be populated with default values from the data source
	 *
	 */
	private $populateForm;



	/**
	 * @var $entityManager
	 *
	 * Sets the entityManager for any operations on the data source
	 *
	 */
	private $entityManager;



	/**
	 * @var $id
	 *
	 * Sets the id of the entity/record this form operates on
	 *
	 */
	private $id;



	/**
	 * @var $action
	 *
	 * Sets the action of the entity/record this form operates on
	 *
	 */
	private $action;



	/**
	 * @method __construct
	 *
	 * Construscts the Datagrid object
	 */
	public function __construct($id = NULL)
	{
		// The default label to append to form elements
		$this->id = htmlentities($id);
		$this->label = 'default';
		$this->showSearch = false;
		$this->populateForm = true;


		// Setting up the id element if specified
		if (!empty($id)) {
			$this->elements = array();
			$this->elements['id']['name'] = 'id';
			$this->elements['id']['label'] = 'ID';
			$this->elements['id']['value'] = $id;
			$this->elements['id']['type'] = 'hidden';
		}
	}



	/**
	 * @method setTitle
	 *
	 * Sets the title of the datagrid
	 *
	 * @param $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}



	/**
	 * @method getTitle
	 *
	 * Gets the title of the datagrid
	 *
	 * @return $title
	 */
	public function getTitle()
	{
		return $this->title;
	}



	/**
	 * @method setDescription
	 *
	 * Sets the description of the datagrid
	 *
	 * @param $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}



	/**
	 * @method getDescription
	 *
	 * Gets the description of the datagrid
	 *
	 * @return $description
	 */
	public function getDescription()
	{
		return $this->description;
	}



	/**
	 * @method setElements
	 *
	 * Sets the elements of the datagrid
	 *
	 * @param $elements
	 */
	public function setElements($elements)
	{
		$this->elements = $elements;
	}



	/**
	 * @method addElements
	 *
	 * Adds elements to the form
	 *
	 * @param $elements
	 */
	public function addElements($elements)
	{
		$this->elements = array_merge($this->elements, $elements);
	}



	/**
	 * @method getElements
	 *
	 * Gets the elements of the datagrid
	 *
	 * @return $elements
	 */
	public function getElements()
	{
		return $this->elements;
	}



	/**
	 * @method setPopulateForm
	 *
	 * Sets the populateForm to indicate weather or not the form
	 * should be populated with default values from the data source
	 *
	 * @param $populateForm
	 */
	public function setPopulateForm($populateForm)
	{
		$this->populateForm = $populateForm;
	}



	/**
	 * @method getPopulateForm
	 *
	 * Gets the populateForm value for the datagrid
	 *
	 * @return $populateForm (true/false)
	 */
	public function getPopulateForm()
	{
		return $this->populateForm;
	}



	/**
	 * @method getEntityManager
	 *
	 * Gets the entityManager for use with operations on the data source
	 *
	 * @return $entityManager
	 */
	public function getEntityManager()
	{
		if (!is_object($this->entityManager)) {
			if (empty($this->driver)) {
				throw new Exception('AWIT_WHMCS_Html_Form:: Driver not set');
			}
			$this->entityManager = new AWIT_WHMCS_Data_Connection(
				new $this->driver($this->config, $this->schema)
			);
		}

		return $this->entityManager;
	}



	/**
	 * @method setEntityManager
	 *
	 * Sets the entityManager for use with operations on the data source
	 *
	 * @return $entityManager
	 */
	public function setEntityManager($entityManager)
	{
		if (is_object($entityManager)) {
			$this->entityManager = $entityManager;
		}
	}



	/**
	 * @method setAction
	 *
	 * Sets the action for the form
	 *
	 * @param $action
	 */
	public function setAction($action)
	{
		$this->action = $action;
	}



	/**
	 * @method getAction
	 *
	 * Gets the action of the form
	 *
	 * @return $action
	 */
	public function getAction()
	{
		return $this->action;
	}



	/**
	 * @method drawTable
	 *
	 * Renders the Datagrid
	 *
	 * @param $title The title for the datagrid
	 * @param $description a Detailed description for the datagrid
	 * @param $items an Array of items/records for the datagrid
	 * @param $headers an Array of items which represent the header
	 *
	 * @return $output The HTML output of the rendered datagrid
	 */
	public function drawForm($title = "", $description = "", $elements = array())
	{

		if (!empty($title)) {
			$this->setTitle($title);
		}

		if (!empty($description)) {
			$this->setDescription($description);
		}

		if (!empty($elements)) {
			$this->setElements($elements);
		}

		// Getting the id passed in via constructor
		$id = NULL;
		if (!empty($this->id)) {
			$id = $this->id;
		} else if (!empty($this->elements) && isset($this->elements['id']['value'])){
			$id = $this->elements['id']['value'];
		}

		// Get form action
		if (!empty($this->action)) {
			$this->elements['formKey']['name'] = 'AWIT_WHMCS_Html_Form_Key';
			$this->elements['formKey']['value'] = $this->action['name'];
			$this->elements['formKey']['type'] = 'hidden';
		}

		// Setting the resource for CakePHP_Rest driver
		if ($this->getEntityManager()->getProvider() instanceof AWIT_WHMCS_Data_Drivers_CakePhpRest) {
			$resourceId = $this->getEntityManager()->getResourceId();
			if (!empty($resourceId)) {
				$this->elements[$resourceId]['name'] = $resourceId;
				$this->elements[$resourceId]['value'] = $this->getEntityManager()->getProvider()->getWriteQueryString();
				$this->elements[$resourceId]['type'] = 'hidden';
			}
		}

		// Reading data from data source to populate form fields
		$data = array();
		if ($this->populateForm === true && $id != NULL) {
			try {
				$em = $this->getEntityManager();
				$data = $em->readData(array('id' => $id));
				if (isset($data[0])) {
					$data = $data[0];
				}
			} catch (Exception $ex) {
				$message = $ex->getMessage();
				if (strpos($message, "::") > -1) {
					$messageParts = explode("::", $message);
					AWIT_WHMCS_Util::logError($messageParts[0], $messageParts[1]);
				} else {
					AWIT_WHMCS_Util::logError("Data Error", $ex->getMessage());
				}
			}
		}

		// Checking if form elements have been specified
		if (empty($this->elements) || empty($this->action['action'])) {
			return false;
		}

		printf("
				<strong>
					<span class='title'>%s</span>
				</strong>
				- %s
				<br/>
				<table class='form' width='100%%' border='0' cellspacing='2' cellpadding='3' style='margin-top: 15px'>
			",
			htmlentities($this->title),
			htmlentities($this->description)
		);

		// Security
		$action = array();
		foreach ($this->action as $key => $value) {
			$action[$key] = htmlentities($value);
		}

		// Set encoding type if any specified
		if (!empty($action['enctype'])) {
			$encTypeText = 'enctype="' . $action['enctype'] . '"';
		} else {
			$encTypeText = '';
		}

		printf('
				<form name="%s" action="%s"	method="%s" %s>
			',
			$action['name'],
			$action['action'],
			$action['method'],
			$encTypeText
		);

		foreach ($this->elements as $key => $elem) {

			// Security
			$e = $elem;
			$elem = array();
			foreach ($e as $k => $v) {
				$elem[$k] = htmlentities($v);
			}

			// Setting defaults if populateForm/data not empty/value not already specified
			if (!isset($elem['value']) && !empty($data)) {
				$elem['value'] = htmlentities($data[$key]);
			}

			if ($elem['type'] == 'text') {
				// Processing text inputs

				printf('
						<tr>
							<td width="130" class="fieldlabel">
								%s
							</td>
							<td class="fieldarea">
								<input type="%s" name="%s" value="%s" size="%s">
							</td>
						</tr>
					',
					$elem['label'],
					$elem['type'],
					$elem['name'],
					$elem['value'],
					$elem['size']
				);

			} else if ($elem['type'] == 'hidden') {
				// Processing hidden inputs
				printf('
						<input type="%s" name="%s" value="%s">
					',
					$elem['type'],
					$elem['name'],
					$elem['value']
				);

			} else if ($elem['type'] == 'password') {
				// Processing password inputs

				printf('
						<tr>
							<td width="130" class="fieldlabel">
								%s
							</td>
							<td class="fieldarea">
								<input type="%s" name="%s" value="%s" size="%s">
							</td>
						</tr>
					',
					$elem['label'],
					$elem['type'],
					$elem['name'],
					$elem['value'],
					$elem['size']
				);

			} else if ($elem['type'] == 'textarea') {
				// Processing textarea inputs

				printf('
						<tr>
							<td width="130" class="fieldlabel">
								%s
							</td>
							<td class="fieldarea">
								<textarea type="%s" name="%s" value="%s" size="%s">
								</textarea>
							</td>
						</tr>
					',
					$elem['label'],
					$elem['type'],
					$elem['name'],
					$elem['value'],
					$elem['size']
				);

			} else if ($elem['type'] == 'date') {
				// Processing date inputs

				printf('
						<tr>
							<td width="130" class="fieldlabel">
								%s
							</td>
							<td class="fieldarea">
								<input type="text" name="%s" value="%s" size="%s" class="datepick">
							</td>
						</tr>
					',
					$elem['label'],
					$elem['name'],
					$elem['value'],
					$elem['size']
				);

			} else if ($elem['type'] == 'select') {
				// Processing select inputs


				// Collecting data from sources if specified
				// If query-string set then check for CakePHP
				if (isset($elem['cake::query-string']) && !empty($elem['cake::query-string'])) {
					$em = $this->getEntityManager();

					if ($em->getProvider() instanceof AWIT_WHMCS_Data_Drivers_CakePhpRest) {
						try {
							// Backing up any previously set query string
							$tmpQueryString = $em->getProvider()->getReadQueryString();
							$tmpPrefixData = $em->getProvider()->getPrefixData();

							// Parsing query string for templated id
							$queryString = $elem['cake::query-string'];
							if (preg_match('/\{(.*)\}/', $queryString, $matches)) {
								$templateKey = $matches[1];
								$templateReplace = $matches[0];
								if (empty($id)) {
									if (is_array($_GET['id'])) {
										$id = $_GET['id'][0];
									}
								}
								$queryString = str_replace('{id}', urlencode(htmlentities($id)), $queryString);
							}

							// Use the specified query string
							// This needs a schema regex key with the matching field to be defined
							$em->getProvider()->setReadQueryString($queryString);
							$data = $em->readData();

							$options = array();
							// Building options
							foreach ($data as $optKey => $opt) {
								// Passing custom object specific fields
								if (isset($elem['cake::option-text']) && !empty($elem['cake::option-text'])) {
									if (isset($elem['cake::option-value']) && !empty($elem['cake::option-value'])) {
										$options[$opt[$elem['cake::option-value']]] =
												htmlentities($opt[$elem['cake::option-text']]);
									} else {
										$options[$opt[$elem['cake::option-text']]] = htmlentities($opt[$elem['cake::option-text']]);
									}
								} else {
									// Accepting 1 demensional array
									$options[$optKey] = htmlentities($opt);
								}
							}

							// Applying options to element
							$elem['options'] = $options;

							// Restore previously set query string
							$em->getProvider()->setReadQueryString($tmpQueryString);
							$em->getProvider()->setPrefixData($tmpPrefixData);
						} catch (Exception $ex) {
							$message = $ex->getMessage();
							if (strpos($message, "::") > -1) {
								$messageParts = explode("::", $message);
								AWIT_WHMCS_Util::logError($messageParts[0], $messageParts[1]);
							} else {
								AWIT_WHMCS_Util::logError("Data Error", $ex->getMessage());
							}
						}
					}
				}


				printf('
						<tr>
							<td width="130" class="fieldlabel">
								%s
							</td>
							<td class="fieldarea">
								<select type="%s" name="%s" value="%s" size="%s">
					',
					$elem['label'],
					$elem['type'],
					$elem['name'],
					$elem['value'],
					$elem['size']
				);

				if (!empty($elem['options'])) {
					foreach ($elem['options'] as $value => $label) {
						$value = htmlentities($value);
						$label = htmlentities($label);
						printf('<option value="%s">%s</option>',$value, $label);
					}
				}

				echo "
						</select>
					</td>
				</tr>
				";

			} else if ($elem['type'] == 'checkbox') {
				// Processing checkbox inputs

				$checkValue = htmlentities(($elem['value'])? 'checked': '');
				sprintf('
						<tr>
							<td width="130" class="fieldlabel">
								%s
							</td>
							<td class="fieldarea">
								<input type="%s" name="%s" %s size="%s">
							</td>
						</tr>
					',
					$elem['label'],
					$elem['type'],
					$elem['name'],
					$checkValue,
					$elem['size']
				);

			}

		}

		echo "</table><br/>";

		foreach ($this->elements as $key => $elem) {
			if ($elem['type'] == 'submit') {
				// Processing password inputs

				// Security
				$e = $elem;
				$elem = array();
				foreach ($e as $k => $v) {
					$elem[$k] = htmlentities($v);
				}

				printf('<input type="submit" value="%s" class="%s" name="%s">', $elem['label'], $elem['class'], $elem['name']);
			}
		}

		echo "</form>";
	}



	/**
	 * @method drawTable
	 *
	 * Renders the Datagrid
	 *
	 * @param $title The title for the datagrid
	 * @param $description a Detailed description for the datagrid
	 * @param $items an Array of items/records for the datagrid
	 * @param $headers an Array of items which represent the header
	 *
	 * @return $output The HTML output of the rendered datagrid
	 */
	public function processForm()
	{
		// Return false if no elements defined
		if (empty($this->elements)) {
			return false;
		}
		$data = array();

		// Catching each element
		foreach ($this->elements as $key => $elem) {
			if ($elem['type'] != 'submit') {
				if (isset($_POST[$elem['name']])) {
					if ($elem['type'] == 'checkbox') {
						$data[$key] = ($_POST[$elem['name']] == 'on')? 1:0;
					} else {
						$data[$key] = $_POST[$elem['name']];
					}
				} else {
					if ($elem['type'] == 'checkbox') {
						$data[$key] = 0;
					} else {
						throw new Exception(
							"AWIT_WHMCS_Html_Form::Configured form parameter was not submitted via POST, [{$elem['name']}]"
						);
					}
				}
			}
		}

		// Writing data to data source
		if (!empty($data)) {
			try {
				$em = $this->getEntityManager();
				//print $em->getProvider()->getCurlCommand($data) ."\n<br/>";
				$res = $em->writeData($data);
				$em->errors($res);
			} catch (Exception $ex) {
				$message = $ex->getMessage();
				if (strpos($message, "::") > -1) {
					$messageParts = explode("::", $message);
					AWIT_WHMCS_Util::logError($messageParts[0], $messageParts[1]);
				} else {
					AWIT_WHMCS_Util::logError("Data Error", $ex->getMessage());
				}
			}
		}

		return $res;
	}



	/**
	 * @method _drawActionButtons
	 * @brief draws action buttons for the action column
	 * This function returns a string which represents
	 * action buttons for the column denoted as "moduleactions"
	 *
	 * @param $actions The expected value for $actions is a | delimited string to determine
	 *                 which action buttons to draw
	 *                 possible buttons are "edit|delete"
	 * @param $id The id of the record on which the button is to act
	 * @param $link The link used to perform the action
	 *
	 * @return The button string as HTML
	 */
	private function _drawActionButtons($actions, $id, $link, $label = "this item") {
		$buttons = "";

		// Security
		$id = htmlentities($id);
		$link = htmlentities($link);
		$label = htmlentities($label);

		$editButton = sprintf('
				<a href="%s&do[]=edit&id=%s" style="text-decoration:none">
					<img src="images/edit.gif" width="16" height="16" border="0" alt="Edit">
				</a>
			',
			$link,
			$id
		);

		$deleteButton = sprintf('
				<a href="#"
					onclick="
						if (confirm(\'Are you sure you want to delete %s\')) {
							location.href = "%s&do[]=delete&id=%s";
						} else {
							return false;
						}
					"
					style="text-decoration:none">

					<img src="images/delete.gif" width="16" height="16" border="0" alt="Delete">

				</a>
			',
			$label,
			$link,
			$id
		);



		// Check for add
		if (strpos($actions, 'edit') > -1) {
			$buttons .= $editButton;
		}

		// Check for delete
		if (strpos($actions, 'delete') > -1) {
			$buttons .= $deleteButton;
		}

		return $buttons;
	}



	/**
	 * @method _drawGlobalActionButtons
	 * @brief draws action buttons which apply to the selected items
	 * This function returns a string which represents
	 * action buttons which will apply items which have been checked
	 *
	 * @param $actions The expected value for $actions is a | delimited string to determine
	 *                 which action buttons to draw
	 *                 possible buttons are "edit|delete"
	 * @param $id The id of the record on which the button is to act
	 * @param $link The link used to perform the action
	 *
	 * @return The button string as HTML
	 */
	private function _drawGlobalActionButtons($actions) {
		$buttons = "";

		$deleteButton = '
			<input type="submit" name="deleteRecords" value="    Delete     " class="btn-danger"
					onclick="return confirm(\'Are you sure you want to delete the selected records?\')">
		';



		// Check for delete
		if (strpos($actions, 'delete') > -1) {
			$buttons .= $deleteButton;
		}

		return $buttons;
	}



	/**
	 * @method drawSearchForm
	 *
	 * @param $searchQuery
	 *
	 * @return $ouput Returns the search form HTML as a string
	 */
	public function drawSearchForm($searchQuery)
	{
		$output = sprintf('
				<div style="float:right;">
					<span style="font-size:larger">Search: </span><input type="text" name="q" value="%s">
					<input type="submit" name="search" value="  Search  " class="btn-success">
				</div>
			',
			htmlentities($searchQuery)
		);
		return $output;
	}



}

