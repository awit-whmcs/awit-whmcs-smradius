<?php
/**
 * AWIT SMRADIUS - Bank Transaction Processing Class
 * Copyright (c) 2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we being used from within whmcs framework for functions like logModuleCall etc.
if (!defined("WHMCS")) {
	die("This file cannot be accessed directly");
}


/**
 * @class AWIT_WHMCS_Html_Datagrid
 *
 * @brief Class to assist with representation and management features
 *        of WHMCS applications.
 *
 * The Datagrid class provides an easy way to draw WHMCS styled tables
 * for representing and displaying controls to manage records from databases
 * or any API based service.
 *
 * The standard WHMCS checkbox/checkall can be enabled for your own
 * list of records.
 *
 */
class AWIT_WHMCS_Html_Datagrid extends AWIT_WHMCS_Data_Connection
{



	/**
	 * @var $entityManager
	 *
	 * Sets the entityManager for any operations on the data source
	 *
	 */
	private $entityManager;



	/**
	 * @var $title The title of the datagrid (optional)
	 */
	private $title;



	/**
	 * @var $description The title of the datagrid (optional)
	 */
	private $description;



	/**
	 * @var $items The title of the datagrid (optional)
	 */
	private $items;



	/**
	 * @var $headers The title of the datagrid (optional)
	 */
	private $headers;



	/**
	 * @var $showCheckbox The title of the datagrid (optional)
	 */
	private $showCheckbox;



	/**
	 * @var $label The title of the datagrid (optional)
	 * This variable is prepended to form elements and can be
	 * changed per instance to allow for multiple datagrid forms to
	 * exist on the same page.
	 *
	 */
	private $label;



	/**
	 * @var $showClientSelect The title of the datagrid (optional)
	 */
	private $showClientSelect;



	/**
	 * @var $validation The title of the datagrid (optional)
	 */
	private $validation;



	/**
	 * @var $link The title of the datagrid (optional)
	 */
	private $link;



	/**
	 * @var $labelKey The title of the datagrid (optional)
	 */
	private $labelKey;



	/**
	 * @var $localActions The title of the datagrid (optional)
	 */
	private $localActions;



	/**
	 * @var $globalActions The title of the datagrid (optional)
	 */
	private $globalActions;



	/**
	 * @var $showSearch weather or not to enable search
	 */
	private $showSearch;



	/**
	 * @var $menuActions the menu actions for the top bar
	 */
	private $menuActions;



	/**
	 * @var $config
	 * The configuration to send to the driver
	 */
	private $config;



	/**
	 * @var $schema
	 * The schema to use to perform data operations on
	 */
	private $schema;



	/**
	 * @var $driver
	 * The driver/class to use when performing data operations
	 */
	private $driver;



	/**
	 * @var $showPagination weather or not to enable pagination
	 */
	private $showPagination;



	/**
	 * @var $customLocalActions custom action buttons for actions column in grid
	 */
	private $customLocalActions;



	/**
	 * @var $customMenuActions custom menu buttons display in the top menu
	 */
	private $customMenuActions;



	/**
	 * @method __construct
	 *
	 * Construscts the Datagrid object
	 */
	public function __construct()
	{
		// The default label to append to form elements
		$this->label = 'default';
		$this->showSearch = false;
	}



	/**
	 * @method getEntityManager
	 *
	 * Gets the entityManager for use with operations on the data source
	 *
	 * @return $entityManager
	 */
	public function getEntityManager()
	{
		if (!is_object($this->entityManager)) {
			if (empty($this->driver)) {
				throw new Exception('AWIT_WHMCS_Html_Form:: Driver not set');
			}
			$this->entityManager = new AWIT_WHMCS_Data_Connection(
				new $this->driver($this->config, $this->schema)
			);
		}

		return $this->entityManager;
	}



	/**
	 * @method setEntityManager
	 *
	 * Sets the entityManager for use with operations on the data source
	 *
	 * @return $entityManager
	 */
	public function setEntityManager($entityManager)
	{
		if (is_object($entityManager)) {
			$this->entityManager = $entityManager;
		}
	}




	/**
	 * @method setTitle
	 *
	 * Sets the title of the datagrid
	 *
	 * @param $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}



	/**
	 * @method getTitle
	 *
	 * Gets the title of the datagrid
	 *
	 * @return $title
	 */
	public function getTitle()
	{
		return $this->title;
	}



	/**
	 * @method setDescription
	 *
	 * Sets the description of the datagrid
	 *
	 * @param $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}



	/**
	 * @method getDescription
	 *
	 * Gets the description of the datagrid
	 *
	 * @return $description
	 */
	public function getDescription()
	{
		return $this->description;
	}



	/**
	 * @method setItems
	 *
	 * Sets the items of the datagrid
	 *
	 * @param $items
	 */
	public function setItems($items)
	{
		$this->items = $items;
	}



	/**
	 * @method getItems
	 *
	 * Gets the items of the datagrid
	 *
	 * @return $items
	 */
	public function getItems()
	{
		return $this->items;
	}



	/**
	 * @method setHeaders
	 *
	 * Sets the headers of the datagrid
	 *
	 * @param $headers
	 */
	public function setHeaders($headers)
	{
		$this->headers = $headers;
	}



	/**
	 * @method getHeaders
	 *
	 * Gets the headers of the datagrid
	 *
	 * @return $headers
	 */
	public function getHeaders()
	{
		return $this->headers;
	}



	/**
	 * @method setShowCheckbox
	 *
	 * Sets weather or not to show checkboxes for the datagrid
	 *
	 * @param $showCheckbox
	 */
	public function setShowCheckbox($showCheckbox)
	{
		$this->showCheckbox = $showCheckbox;
	}



	/**
	 * @method getShowCheckbox
	 *
	 * Gets weather or not showing of checkboxes for the datagrid has
	 * been enabled
	 *
	 * @return $showCheckbox
	 */
	public function getShowCheckbox()
	{
		return $this->showCheckbox;
	}



	/**
	 * @method setLabel
	 *
	 * Sets the label of the datagrid
	 *
	 * @param $label
	 */
	public function setLabel($label)
	{
		$this->label = $label;
	}



	/**
	 * @method getLabel
	 *
	 * Gets the label of the datagrid
	 *
	 * @return $label
	 */
	public function getLabel()
	{
		return $this->label;
	}



	/**
	 * @method setShowClientSelect
	 *
	 * Sets weather or not to display the client select text input
	 * this is a relic of the FNBZA module for which this form functionality
	 * was intended and it will be revisted to allow for a much more flexible
	 * way of presenting form controls per field
	 *
	 * @param $showClientSelect
	 */
	public function setShowClientSelect($showClientSelect)
	{
		$this->showClientSelect = $showClientSelect;
	}



	/**
	 * @method getShowClientSelect
	 *
	 * gets the value of showClientSelect of the datagrid
	 *
	 * @return $showClientSelect
	 */
	public function getShowClientSelect()
	{
		return $this->showClientSelect;
	}



	/**
	 * @method setValidation
	 *
	 * Sets the records to be highlighted for any validation issues
	 * This is a relic of the FNBZA module for which this was intended
	 * and will be incorporated into the widgets meant for displaying validation
	 * errors on a per field basis
	 *
	 * @param $validation An array of id's representing the items that failed validation
	 */
	public function setValidation($validation)
	{
		$this->validation = $validation;
	}



	/**
	 * @method getValidation
	 *
	 * gets the value of items that failed validation (these rows are highlighted)
	 *
	 * @return $validation
	 */
	public function getValidation()
	{
		return $this->validation;
	}



	/**
	 * @method setLink
	 *
	 * Sets the value of items that failed validation (these rows are highlighted)
	 *
	 * @param $link
	 */
	public function setLink($link)
	{
		$this->link = $link;
	}



	/**
	 * @method getLink
	 *
	 * Gets the value of items that failed validation (these rows are highlighted)
	 *
	 * @return $link
	 */
	public function getLink()
	{
		return $this->link;
	}



	/**
	 * @method setLabelKey
	 *
	 * Sets the labelKey
	 *
	 * @param $labelKey
	 */
	public function setLabelKey($labelKey)
	{
		$this->labelKey = $labelKey;
	}



	/**
	 * @method getLabelKey
	 *
	 * Gets the labelKey
	 *
	 * @return $labelKey
	 */
	public function getLabelKey()
	{
		return $this->labelKey;
	}



	/**
	 * @method setLocalActions
	 *
	 * Sets the localActions
	 *
	 * @param $localActions
	 */
	public function setLocalActions($localActions)
	{
		$this->localActions = $localActions;
	}



	/**
	 * @method getLocalActions
	 *
	 * Gets the localActions
	 *
	 * @return $localActions
	 */
	public function getLocalActions()
	{
		return $this->localActions;
	}



	/**
	 * @method setGlobalActions
	 *
	 * Sets the globalActions
	 *
	 * @param $globalActions
	 */
	public function setGlobalActions($globalActions)
	{
		$this->globalActions = $globalActions;
	}



	/**
	 * @method getGlobalActions
	 *
	 * Gets the globalActions
	 *
	 * @return $globalActions
	 */
	public function getGlobalActions()
	{
		return $this->globalActions;
	}



	/**
	 * @method setShowSearch
	 *
	 * Sets the showSearch for enabling the search feature
	 *
	 * @param $showSearch
	 */
	public function setShowSearch($showSearch)
	{
		$this->showSearch = $showSearch;
	}



	/**
	 * @method getShowSearch
	 *
	 * Gets the showSearch value
	 *
	 * @return $globalActions
	 */
	public function getShowSearch()
	{
		return $this->showSearch;
	}



	/**
	 * @method setMenuActions
	 *
	 * Sets the showMenuActions variable for enabling the Buttons
	 *
	 * @param $showMenuActions
	 */
	public function setMenuActions($menuActions)
	{
		$this->menuActions = $menuActions;
	}



	/**
	 * @method getMenuActions
	 *
	 * Sets the getMenuActions variable for enabling the Buttons
	 *
	 * @param $getMenuActions
	 */
	public function getMenuActions($menuActions)
	{
		return $this->menuActions;
	}



	/**
	 * @method setShowPagination
	 *
	 * Sets the showPagination for enabling the search feature
	 *
	 * @param $showPagination
	 */
	public function setShowPagination($showPagination)
	{
		$this->showPagination = $showPagination;
	}



	/**
	 * @method getShowPagination
	 *
	 * Gets the showPagination value
	 *
	 * @return $globalActions
	 */
	public function getShowPagination()
	{
		return $this->showPagination;
	}



	/**
	 * @method setCustomLocalActions
	 *
	 * Sets the customLocalActions for adding custom buttons per record
	 *
	 * @param $customLocalActions
	 */
	public function setCustomLocalActions($customLocalActions)
	{
		$this->customLocalActions = $customLocalActions;
	}



	/**
	 * @method getCustomLocalActions
	 *
	 * Gets the customLocalActions value
	 *
	 * @return $this->customLocalActions
	 */
	public function getCustomLocalActions()
	{
		return $this->customLocalActions;
	}



	/**
	 * @method setCustomMenuActions
	 *
	 * Sets the customMenuActions for adding custom buttons per record
	 *
	 * @param $customMenuActions
	 */
	public function setCustomMenuActions($customMenuActions)
	{
		$this->customMenuActions = $customMenuActions;
	}



	/**
	 * @method getCustomMenuActions
	 *
	 * Gets the customMenuActions value
	 *
	 * @return $this->customMenuActions
	 */
	public function getCustomMenuActions()
	{
		return $this->customMenuActions;
	}



	/**
	 * @method drawDatagrid
	 *
	 * Renders the Datagrid
	 *
	 * @param $title The title for the datagrid
	 * @param $description a Detailed description for the datagrid
	 * @param $items an Array of items/records for the datagrid
	 * @param $headers an Array of items which represent the header
	 *
	 * @return $output The HTML output of the rendered datagrid
	 */
	public function drawDatagrid($title = "", $description = "", $items = array(), $headers = array())
	{
		if (!empty($title)) {
			$this->setTitle($title);
		}

		if (!empty($description)) {
			$this->setDescription($description);
		}

		if (!empty($items)) {
			$this->setItems($items);
		} else {
			// Load data from Datasource
			try {
				$items = $this->getEntityManager()->readData();
				$this->setItems($items);
			} catch (Exception $ex) {
				$message = $ex->getMessage();
				if (strpos($message, "::") > -1) {
					$messageParts = explode("::", $message);
					AWIT_WHMCS_Util::logError($messageParts[0], $messageParts[1]);
				} else {
					AWIT_WHMCS_Util::logError("Data Error", $ex->getMessage());
				}
			}
		}

		if (!empty($headers)) {
			$this->setHeaders($headers);
		}

		// Draw search form
		if ($this->showSearch === true) {
			echo $this->drawSearchForm();
		}

		// Draw menu buttons
		if (!empty($this->menuActions) || !empty($this->customMenuActions)) {
			echo "<div style='float:right;padding-right:100px'>";
			echo $this->_drawMenuButtons();
			echo "</div>";
		}

		printf("
				<strong>
					<span class='title'>%s</span>
				</strong>
				- %s
			",
			htmlentities($this->title),
			htmlentities($this->description)
		);

		// No items so return
		if (empty($this->items)) {
			return false;
		}

		if (!empty($this->headers)) {
			$headerLength = count($this->headers);
		} else {
			$headerLength = count($this->items[0]);
		}

		// Adding module actions if local actions specified
		if (!empty($this->localActions)) {
			// If record config doesn't exist for local actions add it
			if (!in_array('moduleactions', array_keys($this->headers))) {
				$this->headers['moduleactions'] = 'Actions';
			}
		}

		echo "<table class='datatable' width='100%' border='0' cellspacing='1' cellpadding='3' style='padding-top: 8px'>";

		// Draw pagination
		if ($this->showPagination === true) {
			echo "<tr><td colspan='99999' align='center' style='border-bottom:none;background-color:#FFFFFF'>";
			echo $this->_drawPagination($_REQUEST['page']);
			echo "</td></tr>";
		}

		// Print headers from headers array
		if (!empty($this->headers)) {
			echo "<tr>";
			if ($this->showCheckbox) {
				printf("<th width='20'><input type='checkbox' id='checkall1_%s'></th>", htmlentities($this->label));
			}

			if ($this->showClientSelect) {
				echo "<th width='20'>Client ID</th>";
			}

			foreach ($this->headers as $k => $v) {
				echo "<th>" . htmlentities($v) . "</th>";
			}
			echo "</tr>";

		} else {
			// Print headers from items array
			echo "<tr>";
			if ($this->showCheckbox) {
				printf("<th width='20'><input type='checkbox' id='checkall1_%s'></th>", htmlentities($this->label));
			}

			if ($this->showClientSelect) {
				echo "<th width='20'>Client ID</th>";
			}

			foreach ($this->items[0] as $k => $v) {
				echo "<th>" . htmlentities($k) . "</th>";
			}
			echo "</tr>";
		}

		// Print items
		for ($i = 0; ($i < count($this->items)); $i++) {
			echo "<tr>";

			// Apply local actions if specified, can still be specified per record if needed
			if (!empty($this->localActions)) {
				// If record config doesn't exist for local actions add it
				if (!in_array('moduleactions', array_keys($this->items[$i]))) {
					$this->items[$i]['moduleactions'] = $this->localActions;
				}
			}

			foreach ($this->items[$i] as $k => $v) {
				if ($k == "id") {
					// Printing Check All
					if ($this->showCheckbox) {
						printf("
								<td>
									<input type='checkbox' name='selecteditems[]' value='%s' class='checkall_%s'>
								</td>
								",
							htmlentities($v),
							htmlentities($this->label)
						);
					}

					// Client ID Textbox
					if ($this->showClientSelect) {
						printf("
								<td width='20'>
									<input type='text' style='width:25px' name='client_id_%s'>
								</td>
								",
							htmlentities($v)
						);
					}

					// Highlight validation items
					if (in_array($v, $this->validation)) {
						$backgroundColor = "#F2D4CE";
					} else {
						$backgroundColor = false;
					}
				}
			}


			// Associative array mapped to keys
			// If we have headers, print only those columns
			if (!empty($this->headers)) {
				foreach ($this->headers as $hk => $hv) {

					foreach ($this->items[$i] as $k => $v) {
						if ($k == $hk) {
							// Capturing id if any (MUST appear before any module actions)
							if ($k == 'id') {
								$id = $v;
							}

							// Grabbing the value of the column designated as the friendly label for this record
							if ($k == $this->labelKey) {
								$labelFriendly = $v;
							}

							// Printing Action HTML with no modifications
							if ($k == 'moduleactions') {
								if (empty($this->label)) {
									$this->label = 'this item';
								} else {
									$this->label = $this->label;
								}
								$actualValue = $this->_drawActionButtons($v, $id, $this->link, $labelFriendly);
							} else {
								$actualValue = htmlentities($v);
							}

							if ($backgroundColor !== false) {
								echo "<td style='background-color:".htmlentities($backgroundColor)."'>"
										. $actualValue . "</td>";
							} else {
								echo "<td>" . $actualValue . "</td>";
							}
						}
					}
				}
			} else {
				foreach ($this->items[$i] as $k => $v) {
					// Capturing id if any (MUST appear before any module actions)
					if ($k == 'id') {
						$id = $v;
					}

					// Grabbing the value of the column designated as the friendly label for this record
					if ($k == $this->labelKey) {
						$labelFriendly = $v;
					}

					// Printing Action HTML with no modifications
					if ($k == 'moduleactions') {
						if (empty($this->label)) {
							$this->label = 'this item';
						} else {
							$this->label = $this->label;
						}
						$actualValue = $this->_drawActionButtons($v, $id, $this->link, $this->labelFriendly);
					} else {
						$actualValue = htmlentities($v);
					}

					if ($backgroundColor !== false) {
						echo "<td style='background-color:".htmlentities($backgroundColor)."'>" . $actualValue . "</td>";
					} else {
						echo "<td>" . $actualValue . "</td>";
					}
				}
			}

			echo "</tr>";
		}

		// Draw pagination
		if ($this->showPagination === true) {
			echo "<tr><td colspan='99999' align='center' style='border-bottom:none;background-color:#FFFFFF'>";
			echo $this->_drawPagination($_REQUEST['page']);
			echo "</td></tr>";
		}

		echo "</table><br/>";

		if (!empty($this->globalActions) && $this->showCheckbox === true) {
			$globalButtons = $this->_drawGlobalActionButtons($this->globalActions);
			echo "With Selected: " . $globalButtons . "<br/>";
			echo "<br/>";
		}

		echo "<div style='border-bottom: 1px dashed #CCCCCC;width:100%'></div><br/>";

		// Adding select all javascript
		printf("
				<script type='text/javascript'>
					$('#checkall1_%s').click(function() {
						$('.checkall_%s').attr('checked',this.checked);
					});
				</script>
				",
			htmlentities($this->label),
			htmlentities($this->label)
		);

		return $output;
	}



	/**
	 * @method _drawActionButtons
	 * @brief draws action buttons for the action column
	 * This function returns a string which represents
	 * action buttons for the column denoted as "moduleactions"
	 *
	 * @param $actions The expected value for $actions is a | delimited string to determine
	 *                 which action buttons to draw
	 *                 possible buttons are "edit|delete"
	 * @param $id The id of the record on which the button is to act
	 * @param $link The link used to perform the action
	 *
	 * @return The button string as HTML
	 */
	private function _drawActionButtons($actions, $id, $link, $label = "this item") {
		// Handle pagination for action buttons
		if (!empty($_REQUEST['page'])) {
			$link .= '&page=' . $_REQUEST['page'];
		}

		// Security
		$id = htmlentities($id);
		$link = htmlentities($link);
		$label = htmlentities($label);

		$buttons = "";

		$editButton = sprintf('
				<a href="%s&do[]=edit&id[]=%s" style="text-decoration:none" title="Edit">
					<img src="images/edit.gif" width="16" height="16" border="0" alt="Edit">
				</a>
			',
			$link,
			$id
		);

		$deleteButton = sprintf('
				<a href="#"
					onclick="
						if (confirm("Are you sure you want to delete %s")) {
							location.href = "%s&do[]=delete&id[]=%s";
						} else {
							return false;
						}
					"
					style="text-decoration:none" title="Delete">
					<img src="images/delete.gif" width="16" height="16" border="0" alt="Delete">
				</a>
			',
			$label,
			$link,
			$id
		);



		// Check for add
		if (strpos($actions, 'edit') > -1) {
			$buttons .= $editButton;
		}

		// Check for delete
		if (strpos($actions, 'delete') > -1) {
			$buttons .= $deleteButton;
		}

		if (!empty($this->customLocalActions)) {
			foreach ($this->customLocalActions as $btn) {

				// Security
				$b = $btn;
				$btn = array();
				foreach ($b as $key => $value) {
					$btn[$key] = htmlentities($value);
				}

				// Setting default alt text
				if (!isset($btn['alt'])) {
					$btn['alt'] = $btn['title'];
				}

				if (isset($btn['confirm'])) {

					$buttons .= sprintf('
							<a href="#"
								onclick="
									if (confirm(%s)) {
										location.href = "%s&id[]=$id";
									} else {
										return false;
									}
								"
								style="text-decoration:none"
								title="%s">

								<img src="%s" width="16" height="16" border="0" alt="%s">

							</a>
						',
						$btn['confirm'],
						$btn['link'],
						$btn['title'],
						$btn['image'],
						$btn['alt']
					);

				} else {

					$buttons .= sprintf('
							<a href="%s&id[]=$id" style="text-decoration:none" title="%s">
								<img src="%s" width="16" height="16" border="0" alt="%s">
							</a>
						',
						$btn['link'],
						$btn['title'],
						$btn['image'],
						$btn['alt']
					);

				}
			}
		}

		return $buttons;
	}



	/**
	 * @method _drawGlobalActionButtons
	 * @brief draws action buttons which apply to the selected items
	 * This function returns a string which represents
	 * action buttons which will apply items which have been checked
	 *
	 * @param $actions The expected value for $actions is a | delimited string to determine
	 *                 which action buttons to draw
	 *                 possible buttons are "edit|delete"
	 * @param $id The id of the record on which the button is to act
	 * @param $link The link used to perform the action
	 *
	 * @return The button string as HTML
	 */
	private function _drawGlobalActionButtons($actions) {
		$buttons = "";

		$deleteButton = '
			<input type="submit" name="deleteRecords" value="    Delete     " class="btn-danger"
			onclick="return confirm(\'Are you sure you want to delete the selected records?\')">
		';



		// Check for delete
		if (strpos($actions, 'delete') > -1) {
			$buttons .= $deleteButton;
		}

		return $buttons;
	}



	/**
	 * @method _drawActionButtons
	 * @brief draws action buttons for the action column
	 * This function returns a string which represents
	 * action buttons for the column denoted as "moduleactions"
	 *
	 * @param $actions The expected value for $actions is a | delimited string to determine
	 *                 which action buttons to draw
	 *                 possible buttons are "edit|delete"
	 * @param $id The id of the record on which the button is to act
	 * @param $link The link used to perform the action
	 *
	 * @return The button string as HTML
	 */
	private function _drawMenuButtons() {
		if (empty($this->link) || (empty($this->menuActions) && empty($this->customMenuActions))) {
			return false;
		}

		$link = htmlentities($this->link);

		$buttons = "";

		// Check for add
		if (strpos($this->menuActions, 'add') > -1) {
			$addButton = sprintf('
					<input type="button" name="add" value="  Create  " class="btn-success"
					onclick="location.href = \'%s&do[]=add\'">
				',
				$link
			);
			$buttons .= $addButton;
		}

		// Drawing any custom menu actions
		if (!empty($this->customMenuActions)) {
			foreach ($this->customMenuActions as $btn) {

				// Security
				$b = $btn;
				$btn = array();
				foreach ($b as $key => $value) {
					$btn[$key] = htmlentities($value);
				}

				// Setting default alt text
				if (!isset($btn['alt'])) {
					$btn['alt'] = $btn['title'];
				}

				$buttons .= sprintf('
						<input type="button" name="add" value="%s" style="padding-right: 10px" class="%s"
						onclick="location.href = \'%s\'" alt="%s">
					',
					$btn['label'],
					$btn['class'],
					$btn['link'],
					$btn['alt']
				);

			}
		}

		return $buttons;
	}



	/**
	 * @method _drawPagination
	 * @brief draws pagination for the datagrid
	 *
	 * @return The HTML list of paginated links
	 */
	private function _drawPagination($page = 1) {
		if (empty($page)) {
			$page = 1;
		} else {
			$page = htmlentities($page);
		}

		$link = htmlentities($this->link);

		// Get provider
		$provider = $this->getEntityManager()->getProvider();

		$pageTotal = 0;
		$pageLimit = 0;



		// Won't draw pagination if provider doesn't support it
		if (method_exists($provider, 'getPaginationPageTotal')) {
			$pageTotal = htmlentities($provider->getPaginationPageTotal());
		} else {
			return false;
		}

		// Won't draw pagination if provider doesn't support it
		if (method_exists($provider, 'getPaginationPageLimit')) {
			$pageLimit = htmlentities($provider->getPaginationPageLimit());
		} else {
			return false;
		}

		$pageCount = intval(ceil($pageTotal / $pageLimit));

		// Won't render pagination if only 1 page to display
		if ($pageCount <= 1) {
			return false;
		}

		// Construct paginated list of links
		$output = "";

		if ($page > 1) {
			$output .= sprintf('
					<a href="%s&page=%d"> &lt;&lt; </a>
				',
				$link,
				intval($page - 1)
			);
		} else if ($page == 1) {
			$output .= " &lt;&lt; ";
		}

		for ($i = 0; ($i < $pageCount); $i++) {
			$output .= sprintf('
					<a href="%s&page=%d">%d</a> |
				',
				$link,
				intval($i + 1),
				intval($i + 1)
			);
		}

		$output = substr($output, 0, strlen($output) - 2);

		if ($page < $pageCount) {
			$output .= sprintf('
					<a href="%s&page=%d"> &gt;&gt; </a>
				',
				$link,
				intval($page + 1)
			);
		} else if ($page == $pageCount) {
			$output .= " &gt;&gt; ";
		}

		$output .= sprintf('
					&nbsp;&nbsp;&nbsp;&nbsp;Page %s of %s
			',
			$page,
			$pageCount
		);

		return $output;
	}



	/**
	 * @method drawSearchForm
	 *
	 * @param $searchQuery
	 *
	 * @return $ouput Returns the search form HTML as a string
	 */
	public function drawSearchForm($searchQuery)
	{
		$output = sprintf('
				<div style="float:right;">
					<span style="font-size:larger">Search: </span><input type="text" name="q" value="%s">
					<input type="submit" name="search" value="  Search  " class="btn-success">
				</div>
			',
			htmlentities($searchQuery)
		);
		return $output;
	}



}

