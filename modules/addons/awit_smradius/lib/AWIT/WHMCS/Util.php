<?php
/**
 * AWIT SMRADIUS - Bank Transaction Processing Class
 * Copyright (c) 2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Make sure we being used from within whmcs framework for functions like logModuleCall etc.
if (!defined("WHMCS")) {
	die("This file cannot be accessed directly");
}


/**
 * @class AWIT_WHMCS_Util
 */
class AWIT_WHMCS_Util
{



	/**
	 * @var $debugLevel Enables different levels of debugging
	 */
	public static $debugLevel = 0;



	/**
	 * @method debug
	 * Logs errors as HTML for browsers or TEXT for command line
	 *
	 * @param $message The message to print
	 */
	public static function debug($message)
	{
		if (self::$debugLevel <= 0) {
			return false;
		}

		if (php_sapi_name() === 'cli') {
			echo strftime("%Y-%m-%d %h:%m:%s") . "," . str_replace("\n", "", strip_tags($message));
		} else {
			echo "
				<div class='infobox'>
					<strong>
						<span class='title'>Debug</span>
					</strong>
					<br/>".htmlentities($message)."
				</div>
			";
		}
	}



	/**
	 * @method logError
	 * Logs errors as HTML for browsers or TEXT for command line
	 *
	 * @param $title Error title
	 * @param $description Description
	 */
	public static function logError($title, $description)
	{
		if (php_sapi_name() === 'cli') {
			echo strftime("%Y-%m-%d %h:%m:%s") . ",ERROR," . str_replace("\n", "", strip_tags($title)) . "," .
					str_replace("\n", "", strip_tags($description)) . "\n";
		} else {
			echo "
				<div class='errorbox'>
					<strong>
						<span class='title'>".htmlentities($title)."</span>
					</strong>
					<br/>".htmlentities($description)."
				</div>
			";
		}
	}



	/**
	 * @method logInfo
	 * Logs info as HTML for browsers or TEXT for command line
	 *
	 * @param $title Error title
	 * @param $description Description
	 */
	public static function logInfo($title, $description)
	{
		if (php_sapi_name() === 'cli') {
			echo strftime("%Y-%m-%d %h:%m:%s") . ",INFO," . str_replace("\n", "", strip_tags($title)) . "," .
					str_replace("\n", "", strip_tags($description)) . "\n";
		} else {
			echo "
				<div class='infobox'>
					<strong>
						<span class='title'>".htmlentities($title)."</span>
					</strong>
					<br/>".htmlentities($description)."
				</div>
			";
		}
	}



	/**
	 * @method logSuccess
	 * Logs success as HTML for browsers or TEXT for command line
	 *
	 * @param $title Error title
	 * @param $description Description
	 */
	public static function logSuccess($title, $description)
	{
		if (php_sapi_name() === 'cli') {
			echo strftime("%Y-%m-%d %h:%m:%s") . ",SUCCESS," . str_replace("\n", "", strip_tags($title)) . "," .
					str_replace("\n", "", strip_tags($description)) . "\n";
		} else {
			echo "
				<div class='successbox'>
					<strong>
						<span class='title'>".htmlentities($title)."</span>
					</strong>
					<br/>".htmlentities($description)."
				</div>
			";
		}
	}



	/**
	 * @method is_assoc_array
	 * Checks an array for atleast 1 string key
	 *
	 * @return bool weather or not the array is associative
	 */
	public static function is_assoc_array($array) {
		if (!is_array($array)) {
			return false;
		}

		return (bool)count(array_filter(array_keys($array), 'is_string'));
	}



	/**
	 * @method json_last_error_msg
	 * Returns a friendly error message for the given json error code
	 *
	 * @return string $error
	 */
	public static function json_last_error_msg() {
		switch (json_last_error()) {
			default:
				return;
			case JSON_ERROR_DEPTH:
				$error = 'Maximum stack depth exceeded';
				break;
			case JSON_ERROR_STATE_MISMATCH:
				$error = 'Underflow or the modes mismatch';
				break;
			case JSON_ERROR_CTRL_CHAR:
				$error = 'Unexpected control character found';
				break;
			case JSON_ERROR_SYNTAX:
				$error = 'Syntax error, malformed JSON';
				break;
			case JSON_ERROR_UTF8:
				$error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
				break;
		}

		return $error;
	}



}
